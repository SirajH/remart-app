# Remart App for Real Estate Agents

The RemartApp was created for real estate agents to easily manage their properties and transactions. This app serves as an all-in-one solution where agents can effortlessly organise and update their property listings. They can also create requests and offers, making communication with clients and potential buyers quick and easy. 

# Development

As the dev framework the Flutter framework is used. 
Flutter is an open-source UI software development kit created by Google. It's used to develop applications for Android, iOS, Linux, Mac, Windows, Google Fuchsia, and the web from a single codebase. Flutter allows developers to create high-quality, native interfaces and is known for its fast development, expressive and flexible designs, and native performance. It uses the Dart programming language and provides its own widgets, which are critical for fast rendering and customizable designs.

