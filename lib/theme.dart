import 'package:flutter/material.dart';

const primaryColor = Color(0xff2d98da);
const accentColor = Color(0xff45aaf2);

final titleStyle = TextStyle(fontWeight: FontWeight.w600);
final border = BoxDecoration(border: Border.all());
final Color bgColor = Color(0xFFEEEEEE);

final Color lightGray = Color(0xffc2c6d2);

final Color gray = Color(0xff999999);

final Color hrColor = Color(0xffe7e2ea);
final Color darkGray = Color(0xff888888);

final cardHeader = TextStyle(
    fontSize: 18, fontWeight: FontWeight.w500, color: primaryColor);

final cardSubheader = TextStyle(color: Color(0xff888888), fontSize: 12);

final double pad4 = 4.0;
final double pad6 = 6.0;
final double pad8 = 8.0;
final double pad10 = 10.0;
final double pad16 = 16.0;
final double pad = 20.0;

final double icon40 = 40.0;
final double icon20 = 20.0;

final double mainIconWidth = 28.0;
final double mainIconHeight = 28.0;

final linkStyle = TextStyle(color: Color(0xff6AA6FF));

final cardHeaderStyle =
    TextStyle(color: primaryColor, fontSize: 18, fontWeight: FontWeight.w600);

final tariffTitle = TextStyle(color: gray);
final tariffDesc =
    TextStyle(color: primaryColor, fontSize: 16, fontWeight: FontWeight.w700);
final tariffDescGrey =
    TextStyle(color: gray, fontSize: 16, fontWeight: FontWeight.w600);

const balanceStyle =
    TextStyle(color: primaryColor, fontSize: 17, fontWeight: FontWeight.w600);
final balanceStyleGrey =
    TextStyle(color: gray, fontSize: 16, fontWeight: FontWeight.w500);

final mipDateTitle = TextStyle(color: gray);
final mipDateDesc = TextStyle(color: gray, fontWeight: FontWeight.w700);

final packAmountStyle =
    TextStyle(color: primaryColor, fontWeight: FontWeight.w600);

final packPriceStyle =
    TextStyle(fontWeight: FontWeight.w600, color: Colors.black54);

final errorTextStyle = TextStyle(color: gray, fontSize: 16);

final dateStyle =
    TextStyle(color: Colors.black54, fontWeight: FontWeight.w500, fontSize: 16);

final highlightStyle =
    TextStyle(fontWeight: FontWeight.bold, color: primaryColor);
