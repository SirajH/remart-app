import 'package:flutter/material.dart';
import 'package:remart/common/M.dart';
import 'package:remart/model/DetailedLoginResponse.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/Status.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/SharedPrefsService.dart';
import 'package:remart/service/Util.dart';
import 'package:rxdart/rxdart.dart';

import 'MessageBloc.dart';

class LoginBloc {
  LoginBloc() {
    pd('C:LoginBloc');
  }

  final HttpServiceImpl http = HttpServiceImpl();
  final SharedPrefsService sharedPrefs = SharedPrefsService();
  final _msgBloc = MessageBloc();

  final _loginStatus = PublishSubject<Status>();
  final _optStatus = PublishSubject<Status>();
  final _registerStatus = PublishSubject<Status>();

  Observable<Status> get loginStatusStream => _loginStatus.stream;

  Observable<Status> get optStatusStream => _optStatus.stream;

  Observable<Status> get registerStatusStream => _registerStatus.stream;

  login(String login, String password) async {
    DetailedLoginResponse dlr;
    addLoginStatus(Status.LOADING);

    final result = await http.login(login, password);

    if (result == null) return M.unexpected;

    if (result is DetailedLoginResponse) {
      dlr = result;
      await sharedPrefs.addLoginResponse(login, dlr);
//      addLoginStatus(Status.OK);
      http.updateProfile(Realtor(countryId: SessionService.countryId));
      return 'ok';
    } else {
//      addLoginStatus(Status.PENDING);
      return result;
    }
  }

  sendOtp(String msisdn) async {
//    addOtpStatus(Status.LOADING);
//    final StringResponseContainer r = await http.sendOtp(msisdn);
//    String msg = r?.stringResponse?.responseMessage;
//    if (msg != null) {
//      if (msg == 'ok') {
//        addOtpStatus(Status.OK);
//      } else {
//        _msgBloc.showMsg(Msg(msg));
//        addOtpStatus(Status.PENDING);
//      }
//    }
  }

  rememberMe(msisdn, password) {
//    sharedPrefs.setString(Constants.MSISDN, msisdn);
//    sharedPrefs.setString(Constants.PASSWORD, password);
  }

  Future<Map<String, String>> get rememberedCreds async {
//    String msisdn = await sharedPrefs.getString(Constants.MSISDN);
//    String pwd = await sharedPrefs.getString(Constants.PASSWORD);
//    return {Constants.MSISDN: msisdn, Constants.PASSWORD: pwd};
  }

  addLoginStatus(Status status) {
    if (!_loginStatus.isClosed) _loginStatus.sink.add(status);
  }

  addOtpStatus(Status status) async {
    if (!_optStatus.isClosed) _optStatus.sink.add(status);
  }

  dispose() {
    _loginStatus.close();
    _optStatus.close();
    _registerStatus.close();
    pd('D:LoginBloc');
  }

  Future<dynamic> register(String msisdn, String password, String name) async {
    return http.register(msisdn: msisdn, password: password, name: name);
  }
}
