import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/HttpService.dart';
import 'package:rxdart/rxdart.dart';

class GeneralBloc {
  final _http = HttpServiceImpl();

  final _citiesSubject = PublishSubject<List<City>>();
  final _districtsSubject = PublishSubject<List<District>>();
  final _villagesSubject = PublishSubject<List<District>>();
  final _metrosSubject = PublishSubject<List<District>>();

  get citiesStream => _citiesSubject.stream;

  get districtsStream => _districtsSubject.stream;

  get villagesStream => _villagesSubject.stream;

  get metrosStream => _metrosSubject.stream;

  close() {
    _citiesSubject.close();
    _districtsSubject.close();
    _villagesSubject.close();
    _metrosSubject.close();
  }

  Future<List<Country>> getCountries() {
    return _http.getCountries();
  }

  getCities(int countryId) async {
    _citiesSubject.add([]);
    _districtsSubject.add([]);
    final List<City> cities = await _http.getCities(countryId);
    _citiesSubject.add(cities);
    if (cities.length > 0) {
      final firstCity = cities[0];
      getDistricts(firstCity.id);
      getVillages(firstCity.id);
      getMetros(firstCity.id);
    }
  }

  void getDistricts(int cityId) async {
    _districtsSubject.add([]);
    final List<District> list = await _http.getDistricts(cityId);
    _districtsSubject.add(list);
  }

  void getVillages(int cityId) async {
    _villagesSubject.add([]);
    final List<District> list = await _http.getVillages(cityId);
    _villagesSubject.add(list);
  }

  void getMetros(int cityId) async {
    _metrosSubject.add([]);
    final List<District> list = await _http.getMetros(cityId);
    _metrosSubject.add(list);
  }
}
