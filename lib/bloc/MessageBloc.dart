import 'package:remart/model/Messages.dart';
import 'package:remart/model/Msg.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:rxdart/rxdart.dart';

class MessageBloc {
  static final _singletone = MessageBloc._();

  MessageBloc._();

  factory MessageBloc() => _singletone;

  final _messageSubject = PublishSubject<String>();

  final _dialogSubject = PublishSubject();

  Observable<String> get messageStream => _messageSubject.stream;

  Observable get dialogStream => _dialogSubject.stream;

  showMsg(Msg errMsg) {
    if (messages.containsKey(errMsg.msg)) {
      addMessageSubject(tr(messages[errMsg.msg]));
    } else {
      addMessageSubject(errMsg.msg);
    }
  }

  addMessageSubject(msg) {
    if (!_messageSubject.isClosed) _messageSubject.sink.add(msg);
  }

  showDialog(value) {
    addDialogSubject(value);
  }

  addDialogSubject(msg) {
    if (!_dialogSubject.isClosed) _dialogSubject.sink.add(msg);
  }

  dispose() {
    _messageSubject.close();
    _dialogSubject.close();
  }
}