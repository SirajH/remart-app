import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/HttpService.dart';
import 'package:rxdart/rxdart.dart';

class ProfileBloc {
  final _http = HttpServiceImpl();
//  final _realtor = PublishSubject<Realtor>();

//  get stream => _realtor.stream;

  Future<Realtor> getMyProfile() async {
    return _http.getMyProfile();
  }

  Future<bool> updateProfile(Realtor realtor) {
    return _http.updateProfile(realtor);
  }

//  dispose() => _realtor.close();
}