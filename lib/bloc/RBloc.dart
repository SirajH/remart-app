import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/SorguFilter.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/SessionService.dart';
import 'package:rxdart/rxdart.dart';

class RBloc {
  final _http = HttpServiceImpl();
  final _requests = PublishSubject<StreamValue<Sorgu>>();

  Observable<StreamValue<Sorgu>> get stream => _requests.stream;

  List<Sorgu> requests = [];

  addRequests(StreamValue<Sorgu> value) {
    if (!_requests.isClosed) _requests.sink.add(value);
  }

  dispose() => _requests.close();

  Future filterRequests({SorguFilter filter, skip = -1}) async {
    // TODO: show loading
    if (skip == -1) requests = [];
    if (filter == null) filter = SorguFilter(countryId: SessionService.countryId);
    addRequests(StreamValue(data: requests, hasMore: false));

    final res = await _http.filterRequests(filter: filter, skip: skip);
    if (res == null) {
      _requests.sink.add(null);
      return;
    }
    if (res.isEmpty) {
      addRequests(StreamValue(data: requests, hasMore: false));
    } else {
      requests.addAll(res);
      addRequests(StreamValue(data: requests, hasMore: true));
    }
  }
}
