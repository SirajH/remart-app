import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/HttpService.dart';
import 'package:rxdart/rxdart.dart';

class RequestDetailsBloc {
  RequestDetailsBloc() {
    print('C:RequestDetailsBloc');
  }

  final _request = BehaviorSubject<Sorgu>();
  final _http = HttpServiceImpl();

  Observable<Sorgu> get requestStream => _request.stream;

  getSorgu(int id) async {
    Sorgu sorgu = await _http.getRequest(id);
    add(sorgu);
  }

  add(Sorgu sorgu) {
    print('RequestDetailsBloc add $sorgu');
    if (!_request.isClosed)
      _request.sink.add(sorgu);
  }

  dispose() {
    _request.close();
    print('D:RequestDetailsBloc');
  }
}
