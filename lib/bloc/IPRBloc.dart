import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/service/HttpService.dart';
import 'package:rxdart/rxdart.dart';

class IPRBloc {
  final _http = HttpServiceImpl();
  final _requests = PublishSubject<StreamValue>();

  get stream => _requests.stream;

  List<Sorgu> requests = [];

  getRequests({id = -1}) async {
//    print('IPRBloc getRequests $id');
    final res = await _http.getInProgressRequests(requestId: id);
    if (res.isEmpty) {
      _requests.sink.add(StreamValue(data: requests, hasMore: false));
    } else {
      requests.addAll(res);
      _requests.sink.add(StreamValue(data: requests, hasMore: true));
    }
  }

  dispose() => _requests.close();
}
