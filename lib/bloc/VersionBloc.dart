import 'package:launch_review/launch_review.dart';
import 'package:package_info/package_info.dart';
import 'package:remart/common/T.dart';
import 'package:remart/model/AppVersion.dart';
import 'package:remart/model/MessageBody.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:rxdart/rxdart.dart';

class VersionBloc {
  static final _singletone = VersionBloc._();

  VersionBloc._() {
    _versionSubject
        .debounce(Duration(seconds: 2))
        .listen((_) => _publishShowPopup());
  }

  factory VersionBloc() => _singletone;

  final http = HttpServiceImpl();
  bool isShown = false;

  var popupTime = 0;

  // is last popup shown less than 60 minutes ago?
  bool get madeRecently =>
      (DateTime.now().millisecondsSinceEpoch - popupTime) < 60 * 60 * 1000;

  final _versionSubject = PublishSubject();
  final _popupSubject = PublishSubject();

  Observable get versionStrem => _popupSubject.stream;

  void close() {
    _versionSubject.close();
    _popupSubject.close();
  }

  void getVersion() {
    if (!_versionSubject.isClosed) {
      _versionSubject.add(true);
    }
  }

  Future _publishShowPopup() async {
//    print('>>>> _publishShowPopup');
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    AppVersion av = await http.getAppVersion();
    if (av != null) {
      int currentBuildNumber = int.parse(packageInfo.buildNumber);
      int buildNumber = 0;
//      print('>>>>> ${av.version}');
      try {
        buildNumber = int.parse(av.version);
      } catch (e) {}

//      print('>>>>> currentBuildNumber $currentBuildNumber');
//      print('>>>>> buildNumber $buildNumber');

      if (currentBuildNumber < buildNumber &&
          !_popupSubject.isClosed &&
          !isShown &&
          !madeRecently) {
        popupTime = DateTime.now().millisecondsSinceEpoch;
        _popupSubject.add(
          MessageBody(
              title: tr(T.alert),
              content: tr(T.newversionavailable),
              mandatory: av.mandatory,
              onSuccess: () => LaunchReview.launch(
                  iOSAppId: "1470623380", androidAppId: "az.remart.app")),
        );
      }
    }
  }
}
