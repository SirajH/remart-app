import 'dart:async';

import 'package:remart/service/SharedPrefsService.dart';
import 'package:rxdart/rxdart.dart';

class LogoutBloc {
  static final _singletone = LogoutBloc._();
  StreamSubscription subs;

  LogoutBloc._() {
    subs = _logoutEventSubject
        .debounce(Duration(milliseconds: 200))
        .listen((_) async {
      await _sharedPrefsService.removeLoginResponse();
      if (!_logoutSubject.isClosed) _logoutSubject.sink.add(true);
    });
  }

  factory LogoutBloc() {
    return _singletone;
  }

  final _sharedPrefsService = SharedPrefsService();

  final _logoutSubject = PublishSubject<bool>();
  final _logoutEventSubject = PublishSubject<bool>();
  final _errorSubject = PublishSubject<String>();

  Observable<bool> get logoutStream => _logoutSubject.stream;

  Observable<String> get errorStream => _errorSubject.stream;

  logout() {
    if (!_logoutEventSubject.isClosed) _logoutEventSubject.sink.add(true);
  }

  showErrorPage(error) {
    if (!_errorSubject.isClosed) _errorSubject.sink.add(error);
  }

  dispose() {
    _logoutSubject.close();
    _logoutEventSubject.close();
    _errorSubject.close();
    subs.cancel();
  }
}
