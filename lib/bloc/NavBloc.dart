import 'package:remart/model/NavData.dart';
import 'package:rxdart/rxdart.dart';

class NavBloc {
  static final _singletone = NavBloc._();

  NavBloc._();

  factory NavBloc() {
    return _singletone;
  }

  final List<NavData> _history = [NavData(0)];

  NavData currRoute = NavData(0);
  NavData prevRoute = NavData(0);

  final _changeSubject = PublishSubject<NavData>();

  Observable<NavData> get routeChangeStream => _changeSubject.stream;

  changeRoute(NavData nav, {bool force = false}) {
    if (nav.index == currRoute.index && !force) return;

    _history.add(nav);
    prevRoute = currRoute;
    currRoute = nav;

    addChangeSubject(nav);
  }

  bool back() {
    if (_history.length == 1) return true;
    _history.removeLast();
    currRoute = _history.last;
    addChangeSubject(_history.last);
    return false;
  }

  addChangeSubject(history) {
    if(!_changeSubject.isClosed) _changeSubject.sink.add(history);
  }

  dispose() {
//    _history.clear();
//    currRoute = NavData(0);
//    _changeSubject.close();
    print('D:NavBloc');
  }
}
