import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/HttpService.dart';

class AppBloc {

  static final _singletone = AppBloc._();

  AppBloc._();

  factory AppBloc() => _singletone;

}