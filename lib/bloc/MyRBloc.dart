import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/SorguFilter.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/SessionService.dart';
import 'package:rxdart/rxdart.dart';

class MyRBloc {
  final _http = HttpServiceImpl();
  final _requests = PublishSubject<StreamValue>();
  var isDataDirty = false; // if data is dirty then data should be updated

  get stream => _requests.stream;

  List<Sorgu> requests = [];

  addRequests(StreamValue<Sorgu> value) {
    if (!_requests.isClosed) _requests.sink.add(value);
  }

  Future filterRequests({SorguFilter filter, skip = -1}) async {
    // TODO: show loading
    if (skip == -1) requests = [];
    if (filter == null) filter = SorguFilter(countryId: SessionService.countryId);
    _requests.sink.add(StreamValue(data: requests, hasMore: false));
    final res = await _http.filterMyRequests(filter: filter, skip: skip);
    if (res == null) {
      _requests.sink.add(null);
      return;
    }
    if (res.isEmpty) {
      _addRequests(StreamValue(data: requests, hasMore: false));
    } else {
      requests.addAll(res);
      _addRequests(StreamValue(data: requests, hasMore: true));
    }
    isDataDirty = false;
  }

  _addRequests(StreamValue value) {
    if (!_requests.isClosed) _requests.add(value);
  }

  Future<bool> createRequest(Sorgu sorgu) => _http.createRequest(sorgu);

  updateRequest(Sorgu sorgu) => _http.updateRequest(sorgu);

  Future<bool> changeRequestStatus(int id, int status) async =>
      _http.changeRequestStatus(id, status);

  dispose() {
    _requests.close();
  }
}
