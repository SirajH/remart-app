import 'dart:async';

import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/service/HttpService.dart';
import 'package:rxdart/rxdart.dart';

class RealtorsBloc {
  final _http = HttpServiceImpl();
  final _requests = PublishSubject<StreamValue<Realtor>>();
  final _filter = PublishSubject<String>();
  String word = '';

  get stream => _requests.stream;

  List<Realtor> realtors = [];
  StreamSubscription _subs;

  RealtorsBloc() {
    _subs = _filter
        .debounce(Duration(milliseconds: 500))
        .listen((_) => getRealtors(skip: -1, clean: true));
  }

  Future getRealtors({skip = -1, clean = false}) async {
    if (clean) realtors = [];
    final res = await _http.getRealtors(skip: skip, word: word);
    if (res.isEmpty) {
      _requests.sink.add(StreamValue(data: realtors, hasMore: false));
    } else {
      realtors.addAll(res);
      _requests.sink.add(StreamValue(data: realtors, hasMore: true));
    }
  }

  filter(String word) {
    this.word = word;
    _filter.add('');
  }

  dispose() {
    _requests.close();
    _filter.close();
    if (_subs != null) _subs.cancel();
  }
}
