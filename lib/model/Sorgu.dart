import 'package:remart/common/T.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

class Sorgu {
  int id;
  int status;
  int sorguTypeId;
  int dealTypeId;
  int roomsMin;
  int roomsMax;
  int areaMin;
  int areaMax;
  int price;
  int currency;
  int repair;
  bool kupchay;
  bool gas;
  String note;
  City city;
  int cityId;
  Country country;
  int countryId;
  DateTime updated;
  Realtor realtor;
  List<City> districts;
  List<int> districtsIds;
  List<City> villages;
  List<int> villagesIds;
  List<City> metros;
  List<int> metrosIds;

  Sorgu(
      {this.id,
      this.status,
      this.sorguTypeId = 1,
      this.dealTypeId = 1,
      this.roomsMin = 0,
      this.roomsMax = 0,
      this.areaMin = 0,
      this.areaMax = 0,
      this.price,
      this.currency = 1,
      this.repair = 0,
      this.kupchay = false,
      this.gas = false,
      this.note,
      this.city,
      this.country,
      this.realtor,
      this.districts,
      this.districtsIds = const [],
      this.villages,
      this.villagesIds = const [],
      this.metros,
      this.metrosIds = const [],
      this.updated,
      this.cityId,
      this.countryId});

  String get district => districts == null
      ? null
      : List.from(districts.map((x) => x.value)).join(', ');

  String get village => villages == null
      ? null
      : List.from(villages.map((x) => x.value)).join(', ');

  String get metro =>
      metros == null ? null : List.from(metros.map((x) => x.value)).join(', ');

  factory Sorgu.fromJson(Map<String, dynamic> json) => Sorgu(
        id: json["id"] == null ? null : json["id"],
        status: json["status"] == null ? null : json["status"],
        sorguTypeId:
            json["sorgu_type_id"] == null ? null : json["sorgu_type_id"],
        dealTypeId: json["deal_type_id"] == null ? null : json["deal_type_id"],
        roomsMin: json["rooms_min"] == null ? null : json["rooms_min"],
        roomsMax: json["rooms_max"] == null ? null : json["rooms_max"],
        areaMin: json["area_min"] == null ? null : json["area_min"],
        areaMax: json["area_max"] == null ? null : json["area_max"],
        price: json["price"] == null ? null : json["price"],
        currency: json["currency"] == null ? null : json["currency"],
        repair: json["repair"] == null ? null : json["repair"],
        kupchay: json["kupchay"] == null ? null : json["kupchay"] == 1,
        gas: json["gas"] == null ? null : json["gas"] == 1,
        note: json["note"] == null ? null : json["note"],
        updated: json["updated_at"] == null
            ? null
            : DateTime.parse('${json["updated_at"]}Z'),
        // Hackie solution to convert timestamp to UTC. (adding Z in the end)
        city: json["city"] == null ? null : City.fromJson(json["city"]),
        cityId: json["city"] == null ? null : json["city"]["id"],
        country:
            json["country"] == null ? null : Country.fromJson(json["country"]),
        countryId: json["country"] == null ? null : json["country"]["id"],
        realtor: json["user"] == null ? null : Realtor.fromJson(json["user"]),
        districts: json["districts"] == null
            ? null
            : List.from(json["districts"].map((x) => City.fromJson(x))),
        districtsIds: json["districts"] == null
            ? null
            : List.from(json["districts"].map((x) => x['id'])),
        villages: json["villages"] == null
            ? null
            : List.from(json["villages"].map((x) => City.fromJson(x))),
        villagesIds: json["villages"] == null
            ? null
            : List.from(json["villages"].map((x) => x['id'])),
        metros: json["metros"] == null
            ? null
            : List.from(json["metros"].map((x) => City.fromJson(x))),
        metrosIds: json["metros"] == null
            ? null
            : List.from(json["metros"].map((x) => x['id'])),
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};
    if (id != null) json['id'] = id;
    if (sorguTypeId != null && sorguTypeId != 0)
      json['sorgu_type_id'] = sorguTypeId;
    if (dealTypeId != null && dealTypeId != 0)
      json['deal_type_id'] = dealTypeId;
    if (roomsMin != null) json['rooms_min'] = roomsMin;
    if (roomsMax != null) json['rooms_max'] = roomsMax;
    if (areaMin != null) json['area_min'] = areaMin;
    if (areaMax != null) json['area_max'] = areaMax;
    if (price != null) json['price'] = price;
    if (currency != null) json['currency'] = currency;
    if (repair != null) json['repair'] = repair;
    if (kupchay != null) json['kupchay'] = kupchay ? 1 : 0;
    if (gas != null) json['gas'] = gas ? 1 : 0;
    if (cityId != null && cityId != 0) json['city_id'] = cityId;
    if (countryId != null && countryId != 0) json['country_id'] = countryId;
    if (note != null) json['note'] = note;

    if (districtsIds != null) json['districts'] = districtsIds;
    if (villagesIds != null) json['villages'] = villagesIds;
    if (metrosIds != null) json['metros'] = metrosIds;
    return json.length == 0 ? null : json;
  }

  String get cur {
    final map = currencies;
    return currency != null ? map[currency] : map[1];
  }

  Map<String, dynamic> toReadable() {
    final map = <String, dynamic>{};

    if (isSorgu(dealTypeId))
      map[C.price] = Util.addTSStyle(moneyParse(price), suffix: ' $cur');
    else
      map[T.priceo] = Util.addTSStyle(moneyParse(price), suffix: ' $cur');

    map[C.room] = Util.normalizewithTS(roomsMin, roomsMax, suffix: tr(C.rm));
    map[C.area] = Util.normalizewithTS(areaMin, areaMax, suffix: C.m2);
    map[C.city] = '${city.value} (${country.value})';

    if (district != '') map[C.district] = district;
    if (village != '') map[C.village] = village;
    if (metro != '') map[C.metro] = metro;
    if (kupchay) map[C.kupchie] = tr(C.exists);
    if (gas) map[C.gas] = tr(C.exists);
    if (repair == 1) map[C.repair] = tr(C.exists);
    if (repair == 2) map[C.repair] = tr(C.nonexists);

    return map;
  }

  String moneyParse(int price) {
    RegExp reg = RegExp(r'(\d{1,3})(?=(\d{3})+(?!\d))');
    Function mathFunc = (Match match) => '${match[1]},';
    return price.toString().replaceAllMapped(reg, mathFunc);
  }

  @override
  String toString() {
    return 'Sorgu{id: $id, status: $status, sorguTypeId: $sorguTypeId, dealTypeId: $dealTypeId, roomsMin: $roomsMin, roomsMax: $roomsMax, areaMin: $areaMin, areaMax: $areaMax, price: $price, repair: $repair, kupchay: $kupchay, gas: $gas, note: $note, city: $city, country: $country, cityId: $cityId, countryId: $countryId, updated: $updated, realtor: $realtor, districts: $districts, districtsIds: $districtsIds, villages: $villages, villagesIds: $villagesIds, metros: $metros, metrosIds: $metrosIds}';
  }

  static Sorgu clone(Sorgu s) {
    return Sorgu(
      id: s.id,
      status: s.status,
      sorguTypeId: s.sorguTypeId,
      dealTypeId: s.dealTypeId,
      roomsMin: s.roomsMin,
      roomsMax: s.roomsMax,
      areaMin: s.areaMin,
      areaMax: s.areaMax,
      price: s.price,
      repair: s.repair,
      kupchay: s.kupchay,
      gas: s.gas,
      note: s.note,
      currency: s.currency,
//      city: s.city,
      cityId: s.cityId,
//      country: s.country,
      countryId: s.countryId,
//      realtor: s.realtor,
//      districts: s.districts,
      districtsIds: List.from(s.districtsIds),
//      villages: s.villages,
      villagesIds: List.from(s.villagesIds),
//      metros: s.metros,
      metrosIds: List.from(s.metrosIds),
    );
  }
}

class Country {
  int id;
  String az;
  String en;
  String ru;

  Country({
    this.id,
    this.az,
    this.en,
    this.ru,
  });

  factory Country.fromJson(Map<String, dynamic> json) => Country(
        id: json["id"] == null ? null : json["id"],
        az: json["az"] == null ? null : json["az"],
        en: json["en"] == null ? null : json["en"],
        ru: json["ru"] == null ? null : json["ru"],
      );

  @override
  String toString() {
    switch (TranslationService().locale) {
      case 'az':
        return az;
      case 'ru':
        return ru;
      case 'en':
        return en;
      default:
        return az;
    }
  }

  String get value => toString();
}

class City {
  int id;
  String az;
  String en;
  String ru;
  int countryId;

  City({
    this.id,
    this.az,
    this.en,
    this.ru,
    this.countryId,
  });

  factory City.fromJson(Map<String, dynamic> json) => City(
        id: json["id"] == null ? null : json["id"],
        az: json["az"] == null ? null : json["az"],
        en: json["en"] == null ? null : json["en"],
        ru: json["ru"] == null ? null : json["ru"],
        countryId: json["country_id"] == null ? null : json["country_id"],
      );

  String get value => toString();

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "az": az == null ? null : az,
        "en": en == null ? null : en,
        "ru": ru == null ? null : ru,
        "country_id": countryId == null ? null : countryId,
      };

  @override
  String toString() {
    switch (TranslationService().locale) {
      case 'az':
        return az;
      case 'ru':
        return ru;
      case 'en':
        return en;
      default:
        return az;
    }
  }
}

class District {
  int id;
  String az;
  String en;
  String ru;
  int cityId;

  District({
    this.id,
    this.az,
    this.en,
    this.ru,
    this.cityId,
  });

  String get value => toString();

  factory District.fromJson(Map<String, dynamic> json) => District(
        id: json["id"] == null ? null : json["id"],
        az: json["az"] == null ? null : json["az"],
        en: json["en"] == null ? null : json["en"],
        ru: json["ru"] == null ? null : json["ru"],
        cityId: json["city_id"] == null ? null : json["city_id"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "az": az == null ? null : az,
        "en": en == null ? null : en,
        "ru": ru == null ? null : ru,
        "city_id": cityId == null ? null : cityId,
      };

  @override
  String toString() {
    switch (TranslationService().locale) {
      case 'az':
        return az;
      case 'ru':
        return ru;
      case 'en':
        return en;
      default:
        return az;
    }
  }
}

class Realtor {
  int id;
  String name;
  String surname;
  String avatar;
  String phone;
  String phone2;
  String whatsapp;
  String whatsapp2;
  String company;
  String email;
  String about;
  int requestCount;
  int pageViews;
  int trusts;
  int countryId;

  String get fullName => '$name ${surname ?? ''}';

  Realtor({
    this.id,
    this.name,
    this.surname,
    this.avatar,
    this.phone,
    this.phone2,
    this.whatsapp,
    this.whatsapp2,
    this.company,
    this.email,
    this.requestCount,
    this.pageViews,
    this.trusts,
    this.about,
    this.countryId,
  });

  factory Realtor.fromJson(Map<String, dynamic> json) => new Realtor(
        id: json["id"] == null ? null : json["id"],
        name: json["name"] == null ? null : json["name"],
        surname: json["surname"] == null ? null : json["surname"],
        avatar: json["avatar"] == null ? null : json["avatar"],
        phone: json["phone"] == null ? null : json["phone"],
        phone2: json["phone2"] == null ? null : json["phone2"],
        whatsapp: json["whatsapp"] == null ? null : json["whatsapp"],
        whatsapp2: json["whatsapp2"] == null ? null : json["whatsapp2"],
        company: json["company"] == null ? null : json["company"],
        email: json["email"] == null ? null : json["email"],
        requestCount:
            json["request_count"] == null ? null : json["request_count"],
        pageViews: json["page_views"] == null ? null : json["page_views"],
        trusts: json["trusts"] == null ? null : json["trusts"],
        about: json["about"] == null ? null : json["about"],
        countryId: json["country_id"] == null ? null : json["country_id"],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};
    if (id != null) json['id'] = id;
    if (name != null && name != '') json['name'] = name;
    if (surname != null) json['surname'] = surname;
    if (email != null) json['email'] = email;
    if (phone != null && phone != '') json['phone'] = phone;
    if (phone2 != null) json['phone2'] = phone2;
    if (whatsapp != null && whatsapp != '') json['whatsapp'] = whatsapp;
    if (whatsapp2 != null) json['whatsapp2'] = whatsapp2;
    if (company != null) json['company'] = company;
    if (avatar != null) json['avatar'] = avatar;
    if (about != null) json['about'] = about;
    if (countryId != null) json['country_id'] = countryId;
    return json.length == 0 ? null : json;
  }

  static Realtor clone(Realtor r) => Realtor(
        id: r.id,
        name: r.name,
        surname: r.surname,
        email: r.email,
        phone: r.phone,
        phone2: r.phone2,
        whatsapp: r.whatsapp,
        whatsapp2: r.whatsapp2,
        company: r.company,
        about: r.about,
        avatar: r.avatar,
      );

  @override
  String toString() =>
      'Realtor{id: $id, name: $name, surname: $surname, avatar: $avatar, phone: $phone, phone2: $phone2, whatsapp: $whatsapp, whatsapp2: $whatsapp2, company: $company, email: $email, about: $about, requestCount: $requestCount, pageViews: $pageViews, trusts: $trusts, countryId: $countryId}';
}

class InProgress {
  final int id;
  String note;

  InProgress(this.id, {this.note});

  @override
  String toString() {
    return 'InProgress{id: $id, note: $note}';
  }
}
