import 'package:remart/common/T.dart';
import 'package:remart/service/TranslationService.dart';

class RequestStatus {
  static const int active = 1;
  static const int deactive = 0;
  static const int deleted = 2;

  static String value(int id) {
    final values = {
      active: tr(T.active),
      deactive: tr(T.deactive),
      deleted: tr(T.deleted)
    };
    return values[id];
  }
}
