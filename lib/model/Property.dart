import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/User.dart';

class Property {
  int id;
  int status;
  Realtor user;
  int rooms;
  double area;
  int type;

  static bool isMain(String item) {
    return ['rooms', 'area'].contains(item);
  }

  Map<String, dynamic> toJson() => {
        "rooms": rooms,
        "area": area,
      };

  Property(
      {this.id, this.status, this.user, this.rooms, this.area, this.type = 1});
}

enum Status { NEW, SEEN }
