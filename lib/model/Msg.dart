class Msg {
  final String msg;

  Msg(this.msg);

  @override
  String toString() {
    return 'Msg{msg: $msg}';
  }
}
