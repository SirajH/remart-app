import 'package:remart/model/NavData.dart';

class Navs {
  static const int requests = 0;
  static const int myrequests = 1;
  static const int myprofile = 2;
  static const int realtors = 3;
  static const int settings = 4;
  static const int logout = 5;
  static const int error = 6;
  static const int countries = 7;
  static const int language = 8;
}
