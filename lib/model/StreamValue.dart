import 'package:remart/model/Sorgu.dart';

class StreamValue<T> {
  List<T> data;
  bool hasMore;
  // TODO: add Status and make data type of T
  StreamValue({this.data, this.hasMore});

  @override
  String toString() {
    return 'StreamValue{data: $data, hasMore: $hasMore}';
  }

}
