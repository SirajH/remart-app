class NavData {
  final int index;
  Object data;

  NavData(this.index, {this.data});

  @override
  String toString() {
    return 'NavData{index: $index}';
  }
}
