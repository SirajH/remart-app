class AppVersion {
  String version;
  bool mandatory;

  AppVersion({
    this.version,
    this.mandatory,
  });

  factory AppVersion.fromJson(Map<String, dynamic> json) => new AppVersion(
    version: json["version"] == null ? null : json["version"],
    mandatory: json["mandatory"] == null ? null : json["mandatory"] == 1,
  );

  Map<String, dynamic> toJson() => {
    "version": version == null ? null : version,
    "mandatory": mandatory == null ? null : mandatory,
  };
}
