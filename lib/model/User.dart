class User {
  final String fullName;
  final String company;
  final String avatar;
  final String email;
  final String phone;
  final String phone2;
  final String whatsapp;
  final int requestCount;
  final int objectCount;
  final int pageViews;
  final int trusts;

  const User(
      {this.fullName,
      this.company,
      this.avatar,
      this.email,
      this.trusts,
      this.phone,
      this.phone2,
      this.whatsapp,
      this.requestCount,
      this.objectCount,
      this.pageViews});
}
