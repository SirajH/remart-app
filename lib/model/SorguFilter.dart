import 'package:remart/service/SessionService.dart';

class SorguFilter {
  int status;
  int sorguTypeId;
  int dealTypeId;
  int rooms;
  int roomsMax;
  int area;
  int areaMax;
  int price;
  int repair;
  bool kupchay;
  bool gas;
  int cityId;
  int countryId;
  int currency;
  List<int> districtsIds = [];
  List<int> villagesIds = [];
  List<int> metrosIds = [];

  SorguFilter({
    this.status,
    this.sorguTypeId = 0,
    this.dealTypeId = 0,
    this.rooms = 0,
    this.roomsMax = 0,
    this.area = 0,
    this.areaMax = 0,
    this.price,
    this.currency = 1, // TODO: make it 0. If manat is selected in filter, it returns other currencies too
    this.repair = 0,
    this.kupchay = false,
    this.gas = false,
    this.cityId,
    this.countryId
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> json = {};

    if (sorguTypeId != null && sorguTypeId != 0)
      json['sorgu_type_id'] = sorguTypeId;
    if (dealTypeId != null && dealTypeId != 0)
      json['deal_type_id'] = dealTypeId;
    if (rooms != null && rooms != 0) json['rooms'] = rooms;
    if (roomsMax != null && roomsMax != 0) json['rooms_max'] = roomsMax;
    if (area != null && area != 0) json['area'] = area;
    if (areaMax != null && areaMax != 0) json['area_max'] = areaMax;
    if (price != null) json['price'] = price;
    if (currency != null && currency != 1) json['currency'] = currency;
    if (repair != null && repair != 0) json['repair'] = repair;
    if (kupchay != null && kupchay) json['kupchay'] = kupchay ? 1 : 0;
    if (gas != null && gas) json['gas'] = gas ? 1 : 0;
    if (cityId != null && cityId != 0) json['city_id'] = cityId;
    if (countryId != null && countryId != 0) json['country_id'] = countryId;

    if (districtsIds != null && districtsIds.isNotEmpty)
      json['districts'] = districtsIds;
    if (villagesIds != null && villagesIds.isNotEmpty)
      json['villages'] = villagesIds;
    if (metrosIds != null && metrosIds.isNotEmpty) json['metros'] = metrosIds;

    return json.length == 0 ? null : json;
  }

  int get count {
    final filters = toJson();
    if (filters == null) return 0;
    if (countryId == SessionService.countryId) return filters.keys.length - 1;
    return filters.keys.length;
  }

  @override
  String toString() {
    return 'SorguFilter{status: $status, sorguTypeId: $sorguTypeId, dealTypeId: $dealTypeId, rooms: $rooms, roomsMax: $roomsMax, area: $area, areaMax: $areaMax, price: $price, currency: $currency, repair: $repair, kupchay: $kupchay, gas: $gas, cityId: $cityId, countryId: $countryId, districts: $districtsIds, villages: $villagesIds, metros: $metrosIds}';
  }
}
