import 'Sorgu.dart';

class DetailedLoginResponse {
  String token;
  Realtor user;

  DetailedLoginResponse({
    this.token,
    this.user,
  });

  factory DetailedLoginResponse.fromJson(Map<String, dynamic> json) =>
      new DetailedLoginResponse(
        token: json["access_token"] == null ? null : json["access_token"],
        user: json["user"] == null ? null : Realtor.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "access_token": token == null ? null : token,
        "user": user == null ? null : user.toJson(),
      };

  @override
  String toString() {
    return 'DetailedLoginResponse{user: $user}';
  }

}
