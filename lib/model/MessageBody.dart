class MessageBody {
  String title;
  String content;
  bool mandatory;
  final Function onSuccess;

  MessageBody({this.title, this.content, this.onSuccess, this.mandatory});
}