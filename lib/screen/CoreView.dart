import 'dart:async';

import 'package:flutter/material.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/bloc/MessageBloc.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/bloc/VersionBloc.dart';
import 'package:remart/common/ExitDialog.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/SplashScreen.dart';
import 'package:remart/common/T.dart';
import 'package:remart/model/DetailedLoginResponse.dart';
import 'package:remart/model/MessageBody.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/SharedPrefsService.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

import 'CountriesScreen.dart';
import 'ErrorPage.dart';
import 'LanguagePickerScreen.dart';
import 'MyProfileScreen.dart';
import 'MyRequestsScreen.dart';
import 'RealtorsScreen.dart';
import 'RequestsScreen.dart';
import 'SettingScreen.dart';

class CoreView extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;

  CoreView(this.navigatorKey) {
    assert(navigatorKey != null);
  }

  @override
  _CoreViewState createState() => _CoreViewState();
}

class _CoreViewState extends State<CoreView> with WidgetsBindingObserver {
  final navBloc = NavBloc();
  final logoutBloc = LogoutBloc();
  final spFuture = _getSettings();
  StreamSubscription subs;
  StreamSubscription subs2;

  static Future<DetailedLoginResponse> _getSettings() async {
    final sps = SharedPrefsService();
    final ts = TranslationService();
    final lr = await sps.getLoginResponse();
    await sps.getCountry();
    await ts.handleAppLang();
    ts.handleTranslations();
    VersionBloc().getVersion();
    return lr;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    pd('IS:CoreViewState');
    subs = logoutBloc.logoutStream.listen((val) {
      SessionService.logout();
      widget.navigatorKey.currentState.pushReplacementNamed('/login');
    });

    subs2 = logoutBloc.errorStream.listen((val) {
      navBloc.changeRoute(NavData(Navs.error, data: val));
    });
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    subs.cancel();
    subs2.cancel();
    pd('D:CoreViewState');
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      TranslationService().handleTranslations();
      VersionBloc().getVersion();
    }
  }

  @override
  Widget build(BuildContext context) {
    pd('B:CoreViewState');
    return NavProviderIWidget(
      navBloc: navBloc,
      child: FutureBuilder(
          future: spFuture,
          builder: (context, AsyncSnapshot<DetailedLoginResponse> snap) {
//            print('snap $snap');
            if (snap.connectionState == ConnectionState.waiting)
              return SplashScreen();
            if (snap.hasError) return SomethingWrong();
//            if (snap.data == null) {
//              LogoutBloc().logout();
//              return SplashScreen();
//            }
//            if (snap.hasData) {
            return CoreWidget(this.widget.navigatorKey, this.navBloc);
//            }
//            return SplashScreen();
          }),
    );
  }
}

class NavProviderIWidget extends InheritedWidget {
  final NavBloc navBloc;

  NavProviderIWidget({
    this.navBloc,
    Key key,
    @required Widget child,
  })  : assert(child != null),
        super(key: key, child: child);

  static NavProviderIWidget of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(NavProviderIWidget)
        as NavProviderIWidget;
  }

  @override
  bool updateShouldNotify(NavProviderIWidget old) => false;
}

class CoreWidget extends StatefulWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final NavBloc navBloc;

//  final FirebaseAnalyticsObserver observer;

  CoreWidget(this.navigatorKey, this.navBloc) {
    assert(navigatorKey != null);
    pd('C:CoreWidget');
  }

  @override
  _CoreWidgetState createState() => _CoreWidgetState();
}

class _CoreWidgetState extends State<CoreWidget> {
  final logoutBloc = LogoutBloc();
  final messageBloc = MessageBloc();
  final versionBloc = VersionBloc();
  int bottomBarIndex = 0;
  StreamSubscription subs;
  StreamSubscription versionSubs;

//  final FirebaseAnalyticsObserver observer;

  _CoreWidgetState();

  @override
  void initState() {
    pd('IS:CoreWidgetState');
    super.initState();
    subs = widget.navBloc.routeChangeStream.listen((NavData data) {
//      observer.analytics.setCurrentScreen(screenName: getPageUri(data.index));
      setState(() {
        int index = data.index;
        if (index == Navs.error) index = widget.navBloc.prevRoute.index;
        bottomBarIndex = index < 4 ? index : 0;
      });
    });

    listenToAppVersionChange();
  }

  listenToAppVersionChange() {
    versionSubs = versionBloc.versionStrem.listen((msg) {
      Future.delayed(Duration(milliseconds: 200), () async {
        MessageBody body = msg;
        VersionBloc().isShown = true;
        final res = await showDialog(
          context: context,
          barrierDismissible: !(body.mandatory ?? false),
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text(body.title),
              content: Text(body.content),
              actions: [
                FlatButton(
                  child: Text(tr(T.ok)),
                  onPressed: () async => Navigator.pop(context, true),
                ),
              ],
            );
          },
        );
        VersionBloc().isShown = false;
        if (res ?? false) body.onSuccess();
      });
    });
  }

  @override
  void dispose() {
    subs.cancel();
    versionSubs.cancel();
    pd('D:CoreWidgetState');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    NavData currRoute = widget.navBloc.currRoute;
    if (currRoute.index == Navs.error) currRoute = widget.navBloc.prevRoute;
    return ExitDialog(
      bloc: widget.navBloc,
      child: Scaffold(
        body: _getPage(widget.navBloc.currRoute),
      ),
    );
  }

  _getPage(NavData data) {
    if(SessionService.countryId == null)
      return LanguagePickerScreen();

    switch (data.index) {
      case Navs.requests:
        return RequestsScreen();
      case Navs.myrequests:
        return MyRequestsScreen();
      case Navs.myprofile:
        return MyProfileScreen();
      case Navs.realtors:
        return RealtorsScreen();
      case Navs.settings:
        return SettingsScreen();
      case Navs.countries:
        return CountriesScreen();
      case Navs.language:
        return LanguagePickerScreen();
      case Navs.error:
        return ErrorPage(error: data.data);
      default:
        return RequestsScreen();
    }
  }
}
