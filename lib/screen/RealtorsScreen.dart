import 'package:flutter/material.dart';
import 'package:remart/bloc/RealtorsBloc.dart';
import 'package:remart/common/MyBottomNavBar.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/VccDrawer.dart';
import 'package:remart/components/LazyList.dart';
import 'package:remart/components/RFab.dart';
import 'package:remart/components/profile_row.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/screen/FilterView.dart';
import 'package:remart/service/TranslationService.dart';

class RealtorsScreen extends StatefulWidget {
  @override
  _RealtorsScreenScreenState createState() => _RealtorsScreenScreenState();
}

class _RealtorsScreenScreenState extends State<RealtorsScreen> {
  final bloc = RealtorsBloc();

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc.getRealtors();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('B:_RealtorsScreenScreenState');
    return Scaffold(
      appBar: AppBar(title: Text(tr(T.realtors))),
      drawer: VccDrawer(),
      bottomNavigationBar: MyBottomNavBar(index: Navs.realtors),
      body: StreamBuilder(
          stream: bloc.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData)
              return _buildSuggestions(snapshot.data, context);
            if (snapshot.hasError) return SomethingWrong();

            return Center(child: CircularProgressIndicator());
          }),
    );
  }

  Widget _buildSuggestions(StreamValue value, BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 86.0),
          child: RefreshIndicator(
            onRefresh: () => bloc.getRealtors(clean: true),
            child: LazyList(
              data: value.data,
              hasMore: value.hasMore,
              builder: (i) => ProfileRow(user: value.data[i]),
              onMore: () => bloc.getRealtors(skip: value.data.length),
            ),
          ),
        ),
        Material(
          elevation: 2,
          child: Container(
            padding: EdgeInsets.all(12),
            color: Colors.white,
            child: TextField(
              decoration: InputDecoration(
                labelText: tr(T.search),
                icon: Icon(Icons.search),
              ),
              onChanged: (word) => bloc.filter(word),
            ),
          ),
        ),
      ],
    );
  }
}
