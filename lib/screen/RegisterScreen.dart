import 'dart:async';

import 'package:flutter/material.dart';
import 'package:remart/bloc/LoginBloc.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/bloc/MessageBloc.dart';
import 'package:remart/common/MessageBuilder.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/Validator.dart';
import 'package:remart/common/common.dart';
import 'package:remart/model/Msg.dart';
import 'package:remart/model/Status.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

import '../theme.dart';

class RegisterScreen extends StatefulWidget {
  @override
  _RegisterScreenState createState() => new _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  Widget build(BuildContext context) {
    pd('B:RegisterScreen');
    return VccScaffold(child: RegisterView(), localeSelected: localeSelected);
  }

  localeSelected(locale) {
    setState(() {
      TranslationService().changeLocale(locale);
    });
  }
}

class RegisterView extends StatefulWidget {
  @override
  RegisterViewState createState() => RegisterViewState();
}

class RegisterViewState extends State<RegisterView> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  final _loginBloc = LoginBloc();
//  final _logoutBloc = LogoutBloc();
//  final _msgBloc = MessageBloc();

  String _msisdn, _password, _name;

//  var myController = TextEditingController(text: '+994');
  var msisdnCtrl = TextEditingController(text: '+994');
  var passwordCtrl = TextEditingController(text: '');
//  StreamSubscription subs2;
  bool hidePassword = true;
  bool rememberMe = false;

  @override
  void initState() {
    pd('IS:RegisterViewState');

//    subs2 = _logoutBloc.errorStream.listen((String msg) {
//      print('RegisterViewState in errorStream');
//      _msgBloc.showMsg(Msg(msg));
//      _loginBloc.addLoginStatus(Status.PENDING);
//    });

    super.initState();
  }

  @override
  void dispose() {
    pd('D:RegisterViewState');
    _loginBloc.dispose();
//    subs2.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RemartCard(
      child: Padding(
        padding: EdgeInsets.only(left: pad, right: pad, bottom: 10),
        child: Form(
          key: this._formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 10),
              Text(tr(T.register), style: cardHeader),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // TODO: return this if you want country code selectable
//                  Expanded(
//                    flex: 1,
//                    child: TextFormField(
//                      enabled: false,
//                      controller: myController,
//                      decoration: InputDecoration(labelText: ''),
//                    ),
//                  ),
//                  SizedBox(width: 8),
                  Expanded(
                    flex: 5,
                    child: TextFormField(
                      controller: msisdnCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: tr(T.yourmsisdn),
                        suffixIcon: Icon(Icons.phone),
                        helperText: '${tr(T.example)}: +994509998877',
                        alignLabelWithHint: true,
                      ),
                      validator: (value) => Validator.msisdn(value),
                      onSaved: (String value) => _msisdn = value.trim(),
                    ),
                  ),
                ],
              ),
              TextFormField(
                decoration: InputDecoration(
                    labelText: tr(T.name),
                    suffixIcon: Icon(Icons.person, color: Colors.grey)),
                validator: (val) => Validator.name(val),
                onSaved: (String value) => _name = value.trim(),
              ),
              TextFormField(
                obscureText: hidePassword,
                controller: passwordCtrl,
                decoration: InputDecoration(
                    labelText: tr(T.createpassword),
                    suffixIcon: IconButton(
                      icon: Icon(
                          hidePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Colors.grey),
                      onPressed: () {
                        setState(() => hidePassword = !hidePassword);
                      },
                    )),
                validator: (value) => Validator.password(value),
                onSaved: (String value) => this._password = value.trim(),
              ),
              SizedBox(height: pad),
              StreamBuilder(
                stream: _loginBloc.loginStatusStream,
                builder: (context, AsyncSnapshot snapshot) {
                  return RemartButton(
                      horizontalPad: 40,
                      text: tr(T.register),
                      status: snapshot.data,
                      onTap: () => _register(context));
                },
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
//                  Material(
//                    color: Colors.transparent,
//                    child: InkWell(
//                      child: Container(
//                        padding: EdgeInsets.symmetric(
//                            vertical: pad16, horizontal: pad8),
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          children: [
//                            Icon(Icons.input,
//                                color: Color(0xff6AA6FF), size: 16),
//                            SizedBox(width: 6),
//                            Text(
//                              tr(T.fastlogin),
//                              style: linkStyle,
//                            ),
//                          ],
//                        ),
//                      ),
//                      onTap: () {
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                            builder: (context) =>
//                                FastLoginPage(msisdn: msisdnCtrl.text),
//                          ),
//                        );
//                      },
//                    ),
//                  ),
                ],
              ),
              MessageBuilder()
            ],
          ),
        ),
      ),
    );
  }

  _register(BuildContext context) async {
    if (this._formKey.currentState.validate()) {
      this._formKey.currentState.save();
      final res = await _loginBloc.register(_msisdn, _password, _name);
      if (res == 'ok') {
        Navigator.pop(context);
      } else {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(tr(res))));
      }
    }
  }
}
