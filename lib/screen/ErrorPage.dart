import 'package:flutter/material.dart';
import 'package:remart/common/HiderWidget.dart';
import 'package:remart/common/M.dart';
import 'package:remart/common/T.dart';
import 'package:remart/service/TranslationService.dart';

import '../theme.dart';
import 'CoreView.dart';

class ErrorPage extends StatelessWidget {
  final String error;

  ErrorPage({this.error});

  @override
  Widget build(BuildContext context) {
    final navBloc = NavProviderIWidget.of(context).navBloc;
    return Scaffold(
        backgroundColor: bgColor,
        body: Center(child: _handleError(error, navBloc)));
  }

  _handleError(error, navBloc) {
    switch (error) {
      case M.noInternet:
        return _buildContent(
            text: tr(T.checkinternet),
            icon: Icons.signal_wifi_off,
            retry: () => retry(navBloc));

      case M.connectionTimeout:
        return _buildContent(
            text: tr(T.gatewaytimeout),
            icon: Icons.access_time,
            retry: () => retry(navBloc));
      case M.somethingWrong:
        return _buildContent(
            text: tr(T.unexpectederror),
            icon: Icons.mood_bad,
            retry: () => retry(navBloc));
      default:
        return _buildContent(
            text: tr(T.unexpectederror), icon: Icons.error_outline);
    }
  }

  retry(navBloc) {
    navBloc.back();
  }

  _buildContent({text, icon, retry}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(icon, size: icon40, color: gray),
          SizedBox(height: pad6),
          Text(text, style: errorTextStyle),
          HiderWidget(
            visible: retry != null,
            show: () => Column(
                  children: [
                    SizedBox(height: pad),
                    InkWell(
                      child: Text(tr(T.retry) ?? '',
                          style: TextStyle(color: Colors.blue, fontSize: 18)),
                      onTap: retry,
                    )
                  ],
                ),
          ),
        ],
      ),
    );
  }
}
