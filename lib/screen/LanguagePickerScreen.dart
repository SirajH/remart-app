import 'package:flutter/material.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/common/T.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';

class LanguagePickerScreen extends StatefulWidget {
  @override
  _LanguagePickerScreenState createState() => _LanguagePickerScreenState();
}

class _LanguagePickerScreenState extends State<LanguagePickerScreen> {
  _changeLang(lang) {
    print(lang);
    TranslationService().changeLocale(lang);
    SessionService.countryId = 1; // If not set, then it will show lang screen forever
    NavBloc().changeRoute(NavData(Navs.countries), force: true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(T.applang)),
        elevation: 0,
      ),
      body: ListView(
        children: ListTile.divideTiles(
          context: context,
          tiles: [
            for (var i in TranslationService.langs)
              ListTile(
                title: Text(i['value']),
                onTap: () => _changeLang(i['lang']),
              )
          ],
        ).toList(),
      ),
    );
  }
}
