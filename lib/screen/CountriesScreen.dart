import 'package:flutter/material.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/SharedPrefsService.dart';
import 'package:remart/service/TranslationService.dart';

class CountriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => NavBloc().changeRoute(NavData(Navs.language)),
        ),
        title: Text(tr(T.countries)),
      ),
      body: FutureBuilder(
        future: HttpServiceImpl().getCountries(),
        builder: (context, AsyncSnapshot<List<Country>> snap) {
          if (snap.connectionState == ConnectionState.waiting)
            return Center(
              child: CircularProgressIndicator(),
            );
          if (snap.hasError) return SomethingWrong();
          return ListView(
            children: ListTile.divideTiles(
              context: context,
              tiles: [
                for (var i in snap.data)
                  ListTile(
                    title: Text(i.value),
                    onTap: () => _changeCountry(i.id),
                  )
              ],
            ).toList(),
          );
        },
      ),
    );
  }

  _changeCountry(int countryId) {
    SessionService.countryId = countryId;
    SharedPrefsService().setField(SharedPrefsService.country, countryId);
    if (SessionService.isLogged) HttpServiceImpl().updateProfile(Realtor(countryId: countryId));
    NavBloc().changeRoute(NavData(Navs.requests));
  }
}
