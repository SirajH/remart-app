import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/Validator.dart';
import 'package:remart/components/profile_row.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/TranslationService.dart';

class MyProfileEditScreen extends StatefulWidget {
  final Realtor realtor;

  const MyProfileEditScreen({Key key, this.realtor}) : super(key: key);

  @override
  _MyProfileEditScreenState createState() => new _MyProfileEditScreenState();
}

class _MyProfileEditScreenState extends State<MyProfileEditScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Realtor realtor;

  @override
  void initState() {
    super.initState();
    realtor = widget.realtor;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(tr(T.myprofile)), elevation: 0.2),
      body: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Container(
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      ProfileRow(
                        user: realtor,
                        clickable: false,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Column(
                          children: [
                            TextFormField(
                              initialValue: realtor.name,
                              decoration: InputDecoration(labelText: tr(T.name)),
                              onSaved: (v) => realtor.name = v.trim(),
                              validator: (v) => Validator.nonEmpty(v),
                            ),
                            TextFormField(
                              initialValue: realtor.surname,
                              decoration: InputDecoration(labelText: tr(T.surname)),
                              onSaved: (v) => realtor.surname = v.trim(),
                            ),
                            TextFormField(
                              initialValue: realtor.company,
                              decoration: InputDecoration(labelText: tr(T.company)),
                              onSaved: (v) => realtor.company = v.trim(),
                            ),
                            TextFormField(
                              initialValue: realtor.email,
                              keyboardType: TextInputType.emailAddress,
                              decoration: InputDecoration(labelText: tr(T.email)),
                              onSaved: (v) =>
                                  realtor.email = v.trim().toLowerCase(),
                              validator: (v) =>
                                  Validate(v, [Validate.email]).validate(),
                            ),
                            TextFormField(
                              initialValue: realtor.phone,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(labelText: tr(T.phone)),
                              onSaved: (v) => realtor.phone = v.trim(),
                              validator: (v) => Validate(
                                      v, [Validate.required, Validate.msisdn])
                                  .validate(),
                            ),
                            TextFormField(
                              initialValue: realtor.phone2,
                              keyboardType: TextInputType.phone,
                              decoration: InputDecoration(labelText: '${tr(T.phone)} 2'),
                              onSaved: (v) => realtor.phone2 = v.trim(),
                              validator: (v) =>
                                  Validate(v, [Validate.msisdn]).validate(),
                            ),
                            TextFormField(
                              initialValue: realtor.whatsapp,
                              keyboardType: TextInputType.phone,
                              decoration:
                                  InputDecoration(labelText: tr(T.whatsapp)),
                              onSaved: (v) => realtor.whatsapp = v.trim(),
                              validator: (v) => Validate(
                                      v, [Validate.required, Validate.msisdn])
                                  .validate(),
                            ),
                            TextFormField(
                              initialValue: realtor.whatsapp2,
                              keyboardType: TextInputType.phone,
                              decoration:
                                  InputDecoration(labelText: '${tr(T.whatsapp)} 2'),
                              onSaved: (v) => realtor.whatsapp2 = v.trim(),
                              validator: (v) =>
                                  Validate(v, [Validate.msisdn]).validate(),
                            ),
                            TextFormField(
                              initialValue: realtor.about,
                              decoration:
                                  InputDecoration(labelText: tr(T.about)),
                              onSaved: (v) => realtor.about = v.trim(),
                              maxLines: 3,
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 100),
                    ],
                  ),
                ),
              ),
              Builder(
                builder: (context) => Align(
                  alignment: Alignment.bottomCenter,
                  child: SafeArea(
                    child: RaisedButton(
                      color: Colors.red,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 16.0, horizontal: 64),
                        child: Text(
                          tr(T.save),
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      onPressed: () => _onSubmit(context),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) {
    if (_formKey.currentState.validate()) {
      this._formKey.currentState.save();
//      print('_onSubmit: ' + json.encode(realtor.toJson()));
      Navigator.pop(context, realtor);
    } else {
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(tr(T.invalidform))));
    }
  }
}
