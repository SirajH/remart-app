import 'package:flutter/material.dart';
import 'package:remart/bloc/MyRBloc.dart';
import 'package:remart/common/MyBottomNavBar.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/VccDrawer.dart';
import 'package:remart/components/ButtonWithLabel.dart';
import 'package:remart/components/LazyList.dart';
import 'package:remart/components/RequestItem.dart';
import 'package:remart/components/YouShouldLogin.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/SorguFilter.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/screen/AddEditRequestScreen.dart';
import 'package:remart/screen/FilterView.dart';
import 'package:remart/screen/RequestDetailsScreen.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';

class MyRequestsScreen extends StatefulWidget {
  @override
  _MyRequestsScreenState createState() => _MyRequestsScreenState();
}

class _MyRequestsScreenState extends State<MyRequestsScreen> {
  final bloc = MyRBloc();
  var filter = SorguFilter(countryId: SessionService.countryId);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (SessionService.isLogged) bloc.filterRequests(filter: filter);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('B:MyRequestsScreen');
    if (SessionService.isLogged)
      return Scaffold(
        appBar: AppBar(
          title: Text(tr(T.myrequests)),
          actions: [
            ButtonWithLabel(
              tr(T.filter),
              Icons.filter_list,
              page: FilterView(filter: filter),
              count: filter.count,
              onReturn: (SorguFilter val) {
                if (val != null) {
                  filter = val;
                  bloc.filterRequests(filter: filter);
                } else {
                  filter = SorguFilter(countryId: SessionService.countryId);
                  bloc.filterRequests(filter: filter);
                }
              },
            )
          ],
        ),
        drawer: VccDrawer(),
        bottomNavigationBar: MyBottomNavBar(index: Navs.myrequests),
        floatingActionButton: FloatingActionButton(
          onPressed: _addRequest,
          child: Icon(Icons.add, color: Colors.white),
        ),
        body: StreamBuilder(
          stream: bloc.stream,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting)
              return Center(child: CircularProgressIndicator());
            if (snapshot.hasData)
              return _buildSuggestions(snapshot.data, context);
            if (snapshot.hasError) return SomethingWrong();
            if (!snapshot.hasData) return YouShouldLogin();

            return Center(child: CircularProgressIndicator());
          },
        ),
      );
    else
      return Scaffold(
        appBar: AppBar(title: Text(tr(T.myrequests))),
        body: YouShouldLogin(),
        drawer: VccDrawer(),
        bottomNavigationBar: MyBottomNavBar(index: Navs.myrequests),
      );
  }

  Widget _buildSuggestions(StreamValue value, BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => bloc.filterRequests(filter: filter),
      child: LazyList(
      data: value.data,
      hasMore: value.hasMore,
      builder: (i) =>
          RequestItem(
            bloc: bloc,
            isMy: true,
            request: value.data[i],
            onTap: () => _navigatee(context, value.data[i]),
            onEdit: _onEdit,
          ),
      onMore: () {
        bloc.filterRequests(skip: value.data.length, filter: filter);
      },
    ),);
  }

  _navigatee(BuildContext context, Sorgu request) async {
    await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) =>
            RequestDetailsScreen(isMy: true, request: request, bloc: bloc)));
//    print('###### data is ${bloc.isDataDirty? 'dirty': 'notdirty'}');
    if (bloc.isDataDirty) bloc.filterRequests();
  }

  _addRequest() async {
    final sorgu = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => AddEditRequestsScreen()));

    if (sorgu != null) {
      final bool result = await bloc.createRequest(sorgu);
      if (result) {
        bloc.filterRequests(filter: filter);
        Scaffold.of(context).showSnackBar(
            SnackBar(content: Text('${tr(T.request)} ${tr(T.created)}')));
      }
    }
  }

  _onEdit(Sorgu s, {int status}) async {
    if (status == null) {
      final sorgu = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  AddEditRequestsScreen(sorgu: Sorgu.clone(s))));

      if (sorgu != null) {
        bool result = await bloc.updateRequest(sorgu);
        if (result) {
          bloc.filterRequests(filter: filter);
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text('${tr(T.request)} ${tr(T.updated)}')));
        }
      }
    } else {
      bool result = await bloc.changeRequestStatus(s.id, status);
      if (result) {
        Scaffold.of(context)
            .showSnackBar(SnackBar(content: Text(tr(T.statuschanged))));
        bloc.filterRequests(filter: filter);
      }
    }
  }
}
