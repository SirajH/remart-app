import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/common/MessageBuilder.dart';
import 'package:remart/common/MyBottomNavBar.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/VccDrawer.dart';
import 'package:remart/common/common.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/SharedPrefsService.dart';
import 'package:remart/service/TranslationService.dart';

import '../theme.dart';
import 'CoreView.dart';

class SettingsScreen extends StatefulWidget {
  @override
  createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
//  final SettingsBloc bloc = SettingsBloc();

  @override
  void dispose() {
    print('D:SettingsPage');
//    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('B:SettingsPage');
    final NavBloc navBloc = NavProviderIWidget.of(context).navBloc;
    return Scaffold(
      backgroundColor: bgColor,
      appBar: AppBar(title: Text(tr(T.settings))),
      drawer: VccDrawer(),
      bottomNavigationBar: MyBottomNavBar(index: Navs.requests),
      body: SingleChildScrollView(
        child: Column(
          children: [
            AppLang(navBloc: navBloc),
            CountrySelector(),
            MessageBuilder(),
            SizedBox(height: 40),
          ],
        ),
      ),
    );
  }
}

class AppLang extends StatefulWidget {
  final NavBloc navBloc;

  const AppLang({Key key, this.navBloc}) : super(key: key);

  @override
  _AppLangState createState() => _AppLangState();
}

class _AppLangState extends State<AppLang> {
  String currLocale = TranslationService().locale;

  @override
  Widget build(BuildContext context) {
    return VccCardWithHeader(
      headerChild: HeaderContent(
        header: tr(T.applang),
        subHeader: '',
      ),
      child: Container(
        width: double.infinity,
        child: CupertinoSegmentedControl(
          borderColor: primaryColor,
          selectedColor: primaryColor,
          pressedColor: Colors.purple[100],
          groupValue: currLocale,
          children: {
            TranslationService.az: Text('AZ'),
            TranslationService.ru: Text('RU'),
            TranslationService.en: Text('EN'),
          },
          onValueChanged: (locale) {
            TranslationService().changeLocale(locale);
            widget.navBloc.changeRoute(NavData(Navs.settings), force: true);
            setState(() => currLocale = locale);
          },
        ),
      ),
    );
  }
}

class CountrySelector extends StatefulWidget {
  @override
  _CountrySelectorState createState() => _CountrySelectorState();
}

class _CountrySelectorState extends State<CountrySelector> {
  @override
  Widget build(BuildContext context) {
    return VccCardWithHeader(
      headerChild: HeaderContent(
        header: tr(T.countries),
        subHeader: '',
      ),
      child: FutureBuilder(
          future: HttpServiceImpl().getCountries(),
          builder: (context, AsyncSnapshot<List<Country>> snap) {
            if (snap.connectionState == ConnectionState.waiting)
              return Center(
                child: CircularProgressIndicator(),
              );
            if (snap.hasError) return SomethingWrong();
            return Container(
              width: double.infinity,
              child: DropdownButton(
                value: SessionService.countryId,
                isExpanded: true,
                onChanged: _changeCountry,
                items: snap.data
                    .map((i) =>
                        DropdownMenuItem(child: Text(i.value), value: i.id))
                    .toList(),
              ),
            );
          }),
    );
  }

  _changeCountry(countryId) {
    SharedPrefsService().setField(SharedPrefsService.country, countryId);
    SessionService.countryId = countryId;
    if (SessionService.isLogged)
      HttpServiceImpl().updateProfile(Realtor(countryId: countryId));
    setState(() {});
//    SharedPrefsService().remove('country');
  }
}
