import 'package:flutter/material.dart';
import 'package:remart/bloc/ProfileBloc.dart';
import 'package:remart/common/MyBottomNavBar.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/VccDrawer.dart';
import 'package:remart/components/YouShouldLogin.dart';
import 'package:remart/components/info_item_row.dart';
import 'package:remart/components/profile_row.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/screen/MyProfileEditScreen.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';

class MyProfileScreen extends StatefulWidget {
  const MyProfileScreen({Key key}) : super(key: key);

  @override
  _MyProfileScreenState createState() => _MyProfileScreenState();
}

class _MyProfileScreenState extends State<MyProfileScreen> {
  final bloc = ProfileBloc();

  _edit(BuildContext context, Realtor realtor) async {
    final res = await Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => MyProfileEditScreen(
              realtor: Realtor.clone(realtor),
            )));
    if (res == null) return;
    final updated = await bloc.updateProfile(res);
    if (updated)
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(tr(T.updated))));
  }

  @override
  Widget build(BuildContext context) {
    if (SessionService.isLogged)
      return Scaffold(
        appBar: AppBar(title: Text(tr(T.myprofile)), elevation: 0.2),
        drawer: VccDrawer(),
        bottomNavigationBar: MyBottomNavBar(index: Navs.myprofile),
        body: FutureBuilder(
            future: bloc.getMyProfile(),
            builder: (context, AsyncSnapshot<Realtor> snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting)
                return Center(child: CircularProgressIndicator());
              if (snapshot.hasError) return SomethingWrong();

              if (snapshot.hasData) {
                Realtor realtor = snapshot.data;
                return Builder(
                  builder: (context) => Container(
                    child: SingleChildScrollView(
                      child: Column(
                        children: [
                          ProfileRow(
                            user: realtor,
                            actions: [
                              IconButton(
                                  icon: Icon(Icons.edit),
                                  onPressed: () => _edit(context, realtor))
                            ],
                            clickable: false,
                          ),
//                      StatisticsRow({
//                        'Requests': realtor.requestCount.toString(),
////                        'Objects': snapshot.data.objectCount.toString(),
//                        'Page Views': realtor.pageViews.toString()
//                      }),
                          InfoItemRow(
                              subtitle: realtor.fullName,
                              title: '${tr(T.name)} ${tr(T.surname)}'),
                          InfoItemRow(
                              subtitle: realtor.company, title: tr(T.company)),
                          InfoItemRow(
                              subtitle: realtor.email, title: tr(T.email)),
                          InfoItemRow(
                              subtitle: realtor.phone, title: tr(T.phone)),
                          InfoItemRow(
                              subtitle: realtor.phone2,
                              title: '${tr(T.phone)} 2'),
                          InfoItemRow(
                              subtitle: realtor.whatsapp,
                              title: tr(T.whatsapp)),
                          InfoItemRow(
                              subtitle: realtor.whatsapp2,
                              title: '${tr(T.whatsapp)} 2'),
                          InfoItemRow(
                              subtitle: realtor.about, title: tr(T.about)),
                          SizedBox(height: 16),
//              RDelimeter(),
//              SizedBox(height: 8),
//              LinkRow(
//                  title: 'Requests',
//                  count: profile.requestCount,
//                  icon: Icons.assignment,
//                  onTap: () {}),
//              Divider(indent: 70),
//              LinkRow(
//                  title: 'Objects',
//                  count: profile.objectCount,
//                  icon: Icons.business,
//                  onTap: () {}),
//              Divider(indent: 70)
                        ],
                      ),
                    ),
                  ),
                );
              }
              return YouShouldLogin();
            }),
      );
    else
      return Scaffold(
        appBar: AppBar(title: Text(tr(T.myprofile)), elevation: 0.2),
        drawer: VccDrawer(),
        bottomNavigationBar: MyBottomNavBar(index: Navs.myprofile),
        body: YouShouldLogin(),
      );
  }
}
