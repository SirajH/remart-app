import 'package:flutter/material.dart';
import 'package:remart/bloc/MyRBloc.dart';
import 'package:remart/bloc/RequestDetailsBloc.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/VccProgressBarPage.dart';
import 'package:remart/components/BigIconButton.dart';
import 'package:remart/components/CallButton.dart';
import 'package:remart/components/expand_text.dart';
import 'package:remart/components/profile_row.dart';
import 'package:remart/components/whatsapp.dart';
import 'package:remart/model/RequestStatus.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';
import 'package:remart/theme/remart_theme.dart';
import 'package:share/share.dart';

import 'AddEditRequestScreen.dart';

class RequestDetailsScreen extends StatelessWidget {
  final Sorgu request;
  final bool isMy;
  final bool isInProgress;
  final MyRBloc bloc;

  RequestDetailsScreen(
      {Key key,
      this.isMy = false,
      this.isInProgress = false,
      this.request,
      this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    print('B:RequestDetailsScreen');
    return Scaffold(
      backgroundColor: Colors.grey[200],
      body: RequestDetailsView(
          request: request, isMy: isMy, isInProgress: isInProgress, bloc: bloc),
    );
  }
}

class RequestDetailsView extends StatefulWidget {
  final Sorgu request;
  final bool isMy;
  final bool isInProgress;
  final MyRBloc bloc;

  RequestDetailsView(
      {Key key, this.request, this.bloc, this.isMy, this.isInProgress})
      : super(key: key);

  @override
  _RequestDetailsViewState createState() => _RequestDetailsViewState();
}

class _RequestDetailsViewState extends State<RequestDetailsView> {
  final requestDetailsBloc = RequestDetailsBloc();
  final ts = TranslationService();
  Sorgu request;

  _RequestDetailsViewState() {
    print('C:_RequestDetailsViewState');
  }

  @override
  void initState() {
    super.initState();
    requestDetailsBloc.add(widget.request);
  }

  @override
  void dispose() {
    requestDetailsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('B:_RequestDetailsViewState');
    return Scaffold(
        appBar: AppBar(
            title: Text(isSorgu(widget.request.dealTypeId)
                ? tr(T.request)
                : tr(T.offer)),
            actions: [
              PopupMenuButton(
                itemBuilder: (context) => buildPopupMenuItem(context),
                onSelected: (val) => _menuItemSelected(val, context),
                icon: Icon(Icons.more_vert),
              ),
            ]),
        body: _buildBody(context));
    return CustomScrollView(slivers: [
      SliverAppBar(
        title: Text('Request'),
        elevation: 0.5,
        pinned: true,
        actions: [
          PopupMenuButton(
            itemBuilder: (context) => buildPopupMenuItem(context),
            onSelected: (val) => _menuItemSelected(val, context),
            icon: Icon(Icons.more_vert),
          ),
        ],
      ),
      SliverToBoxAdapter(child: _buildBody(context)),
    ]);
  }

//      widget.isInProgress
//          ? SliverPadding(padding: EdgeInsets.only(top: 10))
//          : sliverEmpty,
//      isInProgress
//          ? SliverToBoxAdapter(
//              child: _personalNotes(context),
//            )
//          : _sliverEmpty()

//        isMy ? _sliverEmpty() : _myPropertyHeader(context),
//        isMy
//            ? _sliverEmpty()
//            : SliverToBoxAdapter(child: RDelimeter(height: 8)),
//        isMy ? _sliverEmpty() : _myPropertyList()

//  SliverList _myPropertyList() {
//    return SliverList(
//        delegate: SliverChildBuilderDelegate((builder, i) {
////          offer.rooms = (i / 2).floor();
//      return i % 2 == 1
//          ? SizedBox(height: 8)
//          : PropertyItem(
//              offer: Property(
//                  area: 12, rooms: 1, id: 23, user: widget.request.realtor));
//    }, addSemanticIndexes: true));
//  }

//  SliverToBoxAdapter _myPropertyHeader(BuildContext context) {
//    return SliverToBoxAdapter(
//      child: Material(
//        elevation: 1,
//        child: Container(
//          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
//          child: Row(
//            children: <Widget>[
//              Text(
//                'My Properties',
//                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
//              ),
//              Expanded(
//                child: SizedBox(),
//              ),
//              FlatButton(
//                child: Text('FILTER',
//                    style: TextStyle(color: Theme.of(context).accentColor)),
//                onPressed: () {
//                  Navigator.of(context).push(
//                      MaterialPageRoute(builder: (context) => FilterView()));
//                },
//              ),
//              FlatButton(
//                child: Text('MATCH', style: TextStyle(color: Colors.indigo)),
//                onPressed: () {},
//              )
//            ],
//          ),
//          color: Colors.white,
//        ),
//      ),
//    );
//  }

  List<PopupMenuItem> buildPopupMenuItem(BuildContext context) {
    if (request == null) return null;
    final items = [
      PopupMenuItem(value: C.share, child: Text(tr(T.share))),
    ];

    final myItems = [
      PopupMenuItem(value: C.edit, child: Text(tr(T.edit))),
      PopupMenuItem(value: C.delete, child: Text(tr(T.delete))),
      PopupMenuItem(value: C.share, child: Text(tr(T.share))),
      if (request.status == RequestStatus.active) ...[
        PopupMenuItem(value: C.deactivate, child: Text(tr(T.deactivate)))
      ],
      if (request.status == RequestStatus.deactive) ...[
        PopupMenuItem(value: C.activate, child: Text(tr(T.activate)))
      ],
    ];

    return widget.isMy ? myItems : items;
  }

  void _menuItemSelected(String val, context) {
    switch (val) {
      case C.share:
        _share();
        break;
      case C.edit:
        _edit();
        break;
      case C.activate:
        _changeStatus(context, RequestStatus.active);
        break;
      case C.deactivate:
        _changeStatus(context, RequestStatus.deactive);
        break;
      case C.delete:
        _changeStatus(context, RequestStatus.deleted);
        break;
    }
  }

  _share() => Share.share(_generateShareText());

  _changeStatus(BuildContext context, int status) async {
    final res = await showPrompt(context, tr(T.alert), tr(T.areyousure));
    if (res ?? false) {
      final response =
          await widget.bloc.changeRequestStatus(request.id, status);
      if (response != null && response) {
        widget.bloc.isDataDirty = true; // MyRequest list should be updated
        if (status == RequestStatus.deleted) Navigator.pop(context);
        setState(() => request.status = status);
      }
    }
  }

  _edit() async {
    final sorgu = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) =>
            AddEditRequestsScreen(sorgu: Sorgu.clone(request)),
      ),
    );

    if (sorgu != null) {
      requestDetailsBloc.add(null);
      await widget.bloc.updateRequest(sorgu);
      requestDetailsBloc.getSorgu(request.id);
      widget.bloc.isDataDirty = true; // MyRequest list should be updated
    }
  }

  _generateShareText() {
    final data = request.toReadable();
    final text = data.keys
        .where((i) => data[i] != 'null' && data[i] != null)
        .map((key) =>
            tr(key) +
            ': ' +
            (data[key] is TextSpan ? data[key].toPlainText() : data[key]))
        .toList()
        .join('\n');

    // add fullName
    var fullName = '${request.realtor.fullName}';
    if ((request.realtor.company ?? '') != '')
      fullName += ' (${request.realtor.company})';

    // add phones
    var phones = '${tr(T.phoneshort)}: ${request.realtor.phone}';
    if ((request.realtor.phone2 ?? '') != '')
      phones += ', ${request.realtor.phone2}';

    // add whatsapp
    var whatsapp = '${tr(T.whatsapp)}: ${request.realtor.whatsapp}';
    if ((request.realtor.whatsapp2 ?? '') != '')
      whatsapp += ', ${request.realtor.whatsapp2}';

    final shareText =
        """${isSorgu(request.dealTypeId) ? tr(T.sifarish) : tr(T.offer)}: ${getSorguType(request.sorguTypeId)} - ${getDealType(request.dealTypeId)}
$text
${request.note ?? ''}
$fullName
$phones
$whatsapp""";

    print(shareText);

    return shareText;
  }

  _buildBody(context) {
    return StreamBuilder<Sorgu>(
        stream: requestDetailsBloc.requestStream,
        builder: (context, AsyncSnapshot<Sorgu> snap) {
//          print('>>> requestDetails snap $snap');
          if (snap.connectionState == ConnectionState.waiting ||
              snap.data == null) return VccProgressBarPage();
          if (snap.hasError) return SomethingWrong();
          if (snap.hasData) {
            request = snap.data;
//            return Text('salam');
            return SingleChildScrollView(
              child: Material(
                elevation: 1,
                shadowColor: Color(0xffeeeeee),
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.only(left: 20, top: 20, right: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(getDealType(request.dealTypeId),
                                  style: TextStyle(
                                      color: Colors.black54, fontSize: 12)),
                              Text(
                                getRelativeDate(request.updated),
                                textAlign: TextAlign.right,
                                style: Theme.of(context).textTheme.caption,
                              )
                            ],
                          ),
                          SizedBox(height: 6),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              RichText(
                                text: TextSpan(
                                  children: [
                                    TextSpan(
                                      text: getSorguType(request.sorguTypeId),
                                      style: TextStyle(
                                          color:
                                              typeColors[request.sorguTypeId]),
                                    ),
                                  ],
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                              widget.isMy ? _acDeacButton(context) : SizedBox(),
                            ],
                          ),
                          SizedBox(height: 8),
                          _otherValuesRow(context),
                          SizedBox(height: 6),
                          ExpandText(
                            request.note ?? '',
                            style: TextStyle(height: 1.2, fontSize: 15),
                          ),
//                        SizedBox(height: 16),
//                widget.isMy ? _personalNotes(context) : SizedBox(),
                        ],
                      ),
                    ),
                    if (!widget.isMy)
                      Padding(
                          padding: const EdgeInsets.only(top: 8.0, bottom: 100),
                          child: ProfileRow(
                            user: request.realtor,
                            actions: isMe(request.realtor.id)
                                ? []
                                : [
                                    CallButton(phone: request.realtor.phone),
                                    Whatsapp(
                                      phone: request.realtor.whatsapp,
                                      text: tr(T.whatsapptext),
                                    )
                                  ],
                          )),
                    if (widget.isMy)
                      Padding(
                        padding: const EdgeInsets.only(top: 16.0, bottom: 100),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            BigIconButton(
                              icon: Icons.share,
                              text: tr(T.share),
                              iconSize: 22,
                              onTap: _share,
                            ),
                            if (widget.request.status == RequestStatus.active)
                              BigIconButton(
                                icon: Icons.cancel,
                                text: tr(T.deactivate),
                                iconSize: 22,
                                onTap: () => _changeStatus(
                                    context, RequestStatus.deactive),
                              ),
                            if (widget.request.status == RequestStatus.deactive)
                              BigIconButton(
                                icon: Icons.done,
                                text: tr(T.activate),
                                iconSize: 22,
                                onTap: () => _changeStatus(
                                    context, RequestStatus.active),
                              ),
                            BigIconButton(
                              icon: Icons.delete,
                              text: tr(T.delete),
                              iconSize: 22,
                              onTap: () =>
                                  _changeStatus(context, RequestStatus.deleted),
                            ),
                            BigIconButton(
                              icon: Icons.edit,
                              text: tr(T.edit),
                              iconSize: 22,
                              onTap: _edit,
                            ),
                          ],
                        ),
                      ),
                  ],
                ),
              ),
            );
          }
          return VccProgressBarPage();
        });
  }

  _acDeacButton(context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(
          RequestStatus.value(request.status),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              color: request.status == 1 ? Colors.green : Colors.grey),
        ),
      ),
      onTap: () {
        final int changeTo = request.status == RequestStatus.active
            ? RequestStatus.deactive
            : RequestStatus.active;
        _changeStatus(context, changeTo);
      },
    );
  }

  _otherValuesRow(BuildContext context) {
    final data = request.toReadable();
    final List<TableRow> otherItems = data.keys
        .where((i) => data[i] != 'null' && data[i] != null)
        .map((key) => TableRow(
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: Text(tr(key),
                      style: TextStyle(color: textColor, fontSize: 15)),
                ),
                RichText(
                    text: TextSpan(children: [
                  data[key] is String
                      ? TextSpan(
                          text: data[key],
                          style: TextStyle(fontWeight: FontWeight.w600))
                      : data[key]
                ], style: TextStyle(color: textColor, fontSize: 15)))
              ],
            ))
        .toList();

    return Container(
      width: double.infinity,
      child: Table(
        columnWidths: {0: FractionColumnWidth(0.4)},
        children: otherItems,
      ),
    );
  }

//  _removeFromInP(context) async {
//    bool result = await showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text('Alert'),
//          content: Text('Are you sure?'),
//          actions: [
//            FlatButton(
//              child: Text("YES"),
//              onPressed: () async {
//                Navigator.pop(context, true);
//              },
//            ),
//            FlatButton(
//              child: Text("CANCEL"),
//              onPressed: () => Navigator.pop(context, false),
//            ),
//          ],
//        );
//      },
//    );
//
//    if ((result ?? false) &&
//        await requestsBloc.removeFromInprogress(widget.request.id)) {
//      Scaffold.of(context).showSnackBar(SnackBar(content: Text('')));
//      Navigator.pop(context);
//    }
//  }

//  _takeToWork(context) async {
//    bool result = await showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text("Alert"),
//          content: TakeToWorkWidget(),
//          actions: [
//            FlatButton(
//              child: Text("ADD"),
//              onPressed: () async {
//                bool res = await requestsBloc.makeInProgress(widget.request);
//                // show loading
//                Navigator.pop(context, res);
//              },
//            ),
//            FlatButton(
//              child: Text("CANCEL"),
//              onPressed: () => Navigator.pop(context, false),
//            ),
//          ],
//        );
//      },
//    );
//
//    if (result ?? false)
//      Scaffold.of(context)
//          .showSnackBar(SnackBar(content: Text('Add to in progress')));
//  }
//
//  _editNote(context) async {
////    final cont = TextEditingController(text: request.inP.note);
//    final cont = TextEditingController(text: 'test');
//    bool result = await showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text('Alert'),
//          content: Column(
//            mainAxisSize: MainAxisSize.min,
//            children: [
//              TextField(
//                decoration: InputDecoration(labelText: 'Note'),
//                controller: cont,
//                maxLines: 4,
//              ),
//            ],
//          ),
//          actions: [
//            FlatButton(
//              child: Text("ADD"),
//              onPressed: () async {
//                // show loading
//                bool res = await requestsBloc.updatePersonalNote(
//                    widget.request.id, cont.text);
//                Navigator.pop(context, res);
//              },
//            ),
//            FlatButton(
//              child: Text("CANCEL"),
//              onPressed: () => Navigator.pop(context, false),
//            ),
//          ],
//        );
//      },
//    );

//    if (result)
//      Scaffold.of(context).showSnackBar(SnackBar(content: Text('Added ')));
//  }
}
