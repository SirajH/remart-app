import 'package:flutter/material.dart';
import 'package:remart/common/T.dart';
import 'package:remart/components/CallButton.dart';
import 'package:remart/components/YouShouldLogin.dart';
import 'package:remart/components/info_item_row.dart';
import 'package:remart/components/profile_row.dart';
import 'package:remart/components/whatsapp.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';

class ProfileScreen extends StatelessWidget {
  final Realtor user;

  const ProfileScreen({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (SessionService.isLogged)
      return Scaffold(
        appBar: AppBar(
          title: Text(tr(T.profile)),
          elevation: 0.2,
        ),
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                ProfileRow(
                  user: user,
                  clickable: false,
//                actions: [
//                  BigIconButton(
//                    text: 'Trust',
//                    icon: Icons.group_add,
//                    onTap: () {},
//                  )
//                ],
                ),
//              StatisticsRow({
//                'Requests': user.requestCount.toString(),
//                'Page Views': user.pageViews.toString(),
////                'Trusts': user.trusts.toString()
//              }),
                InfoItemRow(
                    subtitle: user.fullName,
                    title: '${tr(T.name)} ${tr(T.surname)}'),
                InfoItemRow(subtitle: user.company, title: tr(T.company)),
                InfoItemRow(subtitle: user.email, title: tr(T.email)),
                InfoItemRow(
                    subtitle: user.phone,
                    title: tr(T.phone),
                    action: CallButton(
                      phone: user.phone,
                    )),
                InfoItemRow(
                    subtitle: user.phone2,
                    title: tr(T.phone),
                    action: CallButton(
                      phone: user.phone2,
                    )),
                InfoItemRow(
                  subtitle: user.whatsapp,
                  title: tr(T.whatsapp),
                  action:
                      Whatsapp(phone: user.whatsapp, text: tr(T.whatsapptext)),
                ),
                InfoItemRow(
                  subtitle: user.whatsapp2,
                  title: tr(T.whatsapp),
                  action:
                      Whatsapp(phone: user.whatsapp2, text: tr(T.whatsapptext)),
                ),
                InfoItemRow(subtitle: user.about, title: tr(T.about)),
                SizedBox(height: 16),
              ],
            ),
          ),
        ),
      );
    else
      return Scaffold(
        appBar: AppBar(
          title: Text(tr(T.profile)),
          elevation: 0.2,
        ),
        body: YouShouldLogin(),
      );
  }
}
