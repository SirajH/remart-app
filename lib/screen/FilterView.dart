import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:remart/bloc/GeneralBloc.dart';
import 'package:remart/common/T.dart';
import 'package:remart/components/FormUtils.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/SorguFilter.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

class FilterView extends StatefulWidget {
  final filter;

  const FilterView({Key key, this.filter}) : super(key: key);

  @override
  _FilterViewState createState() => _FilterViewState(filter);
}

class _FilterViewState extends State<FilterView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static const pad = 16.0;
  final bloc = GeneralBloc();

  TextEditingController priceController;
  SorguFilter filter;

  Future<List<Country>> fCountries;

  _FilterViewState(this.filter) {
    fCountries = bloc.getCountries();
    bloc.getCities(filter.countryId);
    print('C:_FilterViewState');
    priceController = TextEditingController(
        text: filter.price != null ? '${filter.price}' : '');
    if (filter.dealTypeId == 0) filter.dealTypeId = 1;
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  _onReset() => Navigator.pop(context, null);

  _onFilter(BuildContext context) {
    if (_formKey.currentState.validate()) {
      this._formKey.currentState.save();
      Navigator.pop(context, filter);
    }
  }

  Widget _filterItems(context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: pad),
          _dealType(),
          SizedBox(height: 8),
          _sorguType(),
          _budget(),
          divider,
          _rooms(context),
          _area(context),
          divider,
          _repair(),
          divider,
          _kupchay(),
          divider,
          _gas(),
          divider,
          _countries(context),
          divider,
          _cities(context),
          divider,
          _districts(),
          _villages(),
          _metros(),
          SizedBox(height: 200)
        ],
      ),
    );
  }

  DropdownMenuItem<int> get _egal => DropdownMenuItem(
      child: Text(tr(T.ferqetmez), style: TextStyle(color: Colors.grey)),
      value: 0);

  _dealType() {
    final list = List.generate(7, (int index) => index).map((i) {
      if (i == 0) return _egal;
      return DropdownMenuItem(child: Text(getDealType(i)), value: i);
    }).toList();
    return createList(context, list, tr(T.dealtype), filter.dealTypeId,
        (v) => setState(() => filter.dealTypeId = v));
  }

  _sorguType() {
    final list = List.generate(11, (int index) => index).map((i) {
      if (i == 0) return _egal;
      return DropdownMenuItem(child: Text(getSorguType(i)), value: i);
    }).toList();
    return createList(context, list, tr(T.sorgutype), filter.sorguTypeId,
        (v) => setState(() => filter.sorguTypeId = v));
  }

  _budget() {
    return Row(
      children: [
        budget(
          labelText: isSorgu(filter.dealTypeId) ? tr(T.budgetmin) : tr(T.pricemax),
          controller: priceController,
          onSaved: (String value) {
            filter.price = int.tryParse(value.trim().replaceAll(',', ''));
          },
          required: false,
        ),
        currency(
            value: filter.currency,
            onChange: (v) => setState(() => filter.currency = v)),
      ],
    );
  }

  _rooms(context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(children: [
        Expanded(
          child: createList(
            context,
            genDropDownItems(min: 0, count: 30),
            isSorgu(filter.dealTypeId) ? tr(T.rooms) : tr(T.roomsmin),
            filter.rooms,
            (v) => setState(() {
              filter.rooms = v;
              if (filter.roomsMax < v) filter.roomsMax = v;
            }),
          ),
        ),
        if (!isSorgu(filter.dealTypeId))
          Expanded(
            child: createList(
              context,
              genDropDownItems(min: filter.rooms, count: 20),
              tr(T.roomsmax),
              filter.roomsMax,
              (v) => setState(() => filter.roomsMax = v),
            ),
          ),
      ]),
    );
  }

  _area(context) {
    final pad = 8.0;

    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: pad, right: pad, bottom: pad),
            child: TextFormField(
              controller: TextEditingController(text: '${filter.area}'),
              keyboardType: TextInputType.number,
              inputFormatters: [NumericTextFormatter()],
              decoration: InputDecoration(
                  labelStyle: TextStyle(fontSize: 12),
                  labelText:
                      isSorgu(filter.dealTypeId) ? tr(T.area) : tr(T.areamin)),
              onChanged: (v) {
                var value = 0;
                try {
                  value = int.parse(v);
                } catch (e) {}
                filter.area = value;
                filter.areaMax =
                    filter.areaMax < value ? value : filter.areaMax;
              },
            ),
          ),
        ),
        if (!isSorgu(filter.dealTypeId))
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: pad, right: pad, bottom: pad),
              child: TextFormField(
                controller: TextEditingController(text: '${filter.areaMax}'),
                keyboardType: TextInputType.number,
                inputFormatters: [NumericTextFormatter()],
                decoration: InputDecoration(
                  labelStyle: TextStyle(fontSize: 12),
                  labelText: tr(T.areamax),
                ),
                onChanged: (v) {
                  var value = 0;
                  try {
                    value = int.parse(v);
                  } catch (e) {}
                  filter.areaMax = value;
                },
              ),
            ),
          )
      ]),
    );
  }

  _repair() => repair(
      context: context,
      value: filter.repair,
      onChanged: (v) => setState(() => filter.repair = v));

  _kupchay() => boolItem(
      context: context,
      label: tr(T.cixarish),
      value: filter.kupchay,
      onChanged: (v) => setState(() => filter.kupchay = v));

  _gas() => boolItem(
      context: context,
      label: tr(T.gas),
      value: filter.gas,
      onChanged: (v) => setState(() => filter.gas = v));

  _countries(context) => countries(
      context: context,
      future: fCountries,
      value: filter.countryId,
      onChanged: (v) {
        bloc.getCities(v);

        setState(() {
          filter.countryId = v;
          filter.cityId = 0;
          filter.districtsIds = [];
          filter.villagesIds = [];
          filter.metrosIds = [];
        });
      });

  _cities(context) => cities(
        context: context,
        stream: bloc.citiesStream,
        value: filter.cityId,
        onData: (data) {
          if (data.length > 0 &&
              (filter.cityId == 0 || filter.cityId == null)) {
            filter.cityId = data[0].id;
          }
        },
        onChanged: (v) {
          bloc.getDistricts(v);
          bloc.getVillages(v);
          bloc.getMetros(v);

          setState(() {
            filter.cityId = v;
            filter.districtsIds = [];
            filter.villagesIds = [];
            filter.metrosIds = [];
          });
        },
      );

  _districts() => MultiSelect(
      label: tr(T.districts),
      stream: bloc.districtsStream,
      initialValue: filter.districtsIds,
      onChanged: (v) => setState(() => filter.districtsIds = v));

  _villages() => MultiSelect(
        stream: bloc.villagesStream,
        label: tr(T.villages),
        initialValue: filter.villagesIds,
        onChanged: (v) => setState(() => filter.villagesIds = v),
      );

  _metros() => MultiSelect(
        stream: bloc.metrosStream,
        label: tr(T.metros),
        initialValue: filter.metrosIds,
        onChanged: (v) => setState(() => filter.metrosIds = v),
      );

  @override
  Widget build(BuildContext context) {
    print('B:FilterView');
    return Scaffold(
      appBar: AppBar(
        title: Text(tr(T.filter)),
        elevation: 0.5,
        actions: [
          FlatButton(
            child: Text(tr(T.reset), style: TextStyle(color: Colors.white)),
            onPressed: _onReset,
          ),
        ],
        leading: IconButton(
            icon: Icon(Icons.clear), onPressed: () => Navigator.pop(context)),
      ),
      body: GestureDetector(
        // TODO: hacky solution. Find better way of unfocusing textfield
        behavior: HitTestBehavior.opaque,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Stack(
          children: [
            SingleChildScrollView(
              child: _filterItems(context),
            ),
            SafeArea(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 4.0),
                  child: RaisedButton(
                      color: Theme.of(context).accentColor,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          tr(T.showresults),
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      onPressed: () => _onFilter(context)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
