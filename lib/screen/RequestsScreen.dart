import 'package:flutter/material.dart';
import 'package:remart/bloc/RBloc.dart';
import 'package:remart/common/MyBottomNavBar.dart';
import 'package:remart/common/SomethingWrong.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/VccDrawer.dart';
import 'package:remart/components/LazyList.dart';
import 'package:remart/components/RFab.dart';
import 'package:remart/components/RequestItem.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/SorguFilter.dart';
import 'package:remart/model/StreamValue.dart';
import 'package:remart/screen/FilterView.dart';
import 'package:remart/screen/RequestDetailsScreen.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';

class RequestsScreen extends StatefulWidget {
  @override
  _RequestsScreenState createState() => _RequestsScreenState();
}

class _RequestsScreenState extends State<RequestsScreen> {
  final bloc = RBloc();
  var filter = SorguFilter(countryId: SessionService.countryId);

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    bloc.filterRequests(filter: filter);
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print('B:RequestsScreen');
    return Scaffold(
      appBar: AppBar(title: Text(tr(T.requests))),
      drawer: VccDrawer(),
      bottomNavigationBar: MyBottomNavBar(index: Navs.requests),
      floatingActionButton: RFab(
        tr(T.filter),
        Icons.filter_list,
        page: FilterView(filter: filter),
        onReturn: (SorguFilter val) {
          if (val != null) {
            filter = val;
            bloc.filterRequests(filter: filter);
          } else {
            filter = SorguFilter(countryId: SessionService.countryId);
            bloc.filterRequests(filter: filter);
          }
        },
        count: filter.count,
      ),
      body: StreamBuilder(
          stream: bloc.stream,
          builder: (context, AsyncSnapshot<StreamValue<Sorgu>> snapshot) {
//            print('${snapshot.connectionState}');
            if (snapshot.hasData)
              return _buildSuggestions(snapshot.data, context);
            if (snapshot.hasError) return SomethingWrong();

            return Center(child: CircularProgressIndicator());
          }),
    );
  }

  Widget _buildSuggestions(StreamValue<Sorgu> value, BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => bloc.filterRequests(filter: filter),
      child: LazyList(
        data: value.data,
        hasMore: value.hasMore,
        builder: (i) => RequestItem(
//            isMy: value.data[i].realtor.id == SessionService.id,
          request: value.data[i],
          onTap: () => _navigatee(context, value.data[i]),
        ),
        onMore: () {
//        print('LazyList onMore ' + value.data.length.toString());
          bloc.filterRequests(filter: filter, skip: value.data.length);
        },
      ),
    );
  }

  _navigatee(BuildContext context, Sorgu request) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => RequestDetailsScreen(request: request)));
  }
}
