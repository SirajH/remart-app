import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remart/bloc/GeneralBloc.dart';
import 'package:remart/common/T.dart';
import 'package:remart/components/FormUtils.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

class AddEditRequestsScreen extends StatefulWidget {
  final filter;
  final Sorgu sorgu;

  const AddEditRequestsScreen({Key key, this.filter, this.sorgu})
      : super(key: key);

  @override
  createState() => _AddEditRequestsScreenState();
}

class _AddEditRequestsScreenState extends State<AddEditRequestsScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static const pad = 16.0;
  final bloc = GeneralBloc();
  Sorgu sorgu;
  bool isUpdate = false;

  TextEditingController priceController;
  TextEditingController noteController;

  Future<List<Country>> fCountries;

  _AddEditRequestsScreenState() {
    print('C:_AddEditRequestsViewState');
    fCountries = bloc.getCountries();
  }

  @override
  void dispose() {
    bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    print('IS:_AddEditRequestsViewState');
    if (widget.sorgu != null) {
      bloc.getCities(widget.sorgu.countryId);
      isUpdate = true;
      sorgu = widget.sorgu;
    } else {
      bloc.getCities(SessionService.countryId);
      sorgu = Sorgu(countryId: SessionService.countryId);
    }

    sorgu.roomsMin = sorgu.roomsMin ?? 0;
    sorgu.roomsMax = sorgu.roomsMax ?? 0;
    sorgu.areaMin = sorgu.areaMin ?? 0;
    sorgu.areaMax = sorgu.areaMax ?? 0;

    priceController = TextEditingController(
        text: sorgu.price != null ? '${sorgu.price}' : '');
    noteController =
        TextEditingController(text: sorgu.note != null ? '${sorgu.note}' : '');
  }

  _onReset() => setState(() {
        sorgu = Sorgu(countryId: SessionService.countryId);
        priceController.text = '';
      });

  _onSubmit(BuildContext context) {
    this._formKey.currentState.save();
    if (_formKey.currentState.validate()) {
      if (!isSorgu(sorgu.dealTypeId)) {
        sorgu.areaMax = sorgu.areaMin;
        sorgu.roomsMax = sorgu.roomsMin;
      }
      Navigator.pop(context, sorgu);
    } else {
      String err = tr(T.invalidform);
      if (sorgu.price == null || sorgu.price == 0)
        err += ', ${tr(T.inputbudget)}';
      if (sorgu.districtsIds.isEmpty) err += ', ${tr(T.districtsnotempty)}';
      Scaffold.of(context).showSnackBar(SnackBar(content: Text(err)));
    }
  }

  Widget _filterItems(context) {
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: pad),
          _dealType(context),
          SizedBox(height: 12),
          divider,
          _sorguType(),
          divider,
          _budget(),
          divider,
          _countries(context),
          divider,
          _cities(context),
          divider,
          _districts(),
          _rooms(context),
          divider,
          _area(context),
          divider,
          _repair(),
          divider,
          _kupchay(),
          divider,
          _gas(),
          divider,
          _villages(),
          _metros(),
          _note(),
          SizedBox(height: 200)
        ],
      ),
    );
  }

  DropdownMenuItem<int> get _egal => DropdownMenuItem(
      child: Text(tr(T.ferqetmez), style: TextStyle(color: Colors.grey)),
      value: 0);

  _dealType(BuildContext context) {
    final list = List.generate(6, (int index) => index).map((i) {
      return DropdownMenuItem(child: Text(getDealType(i + 1)), value: i + 1);
    }).toList();
    return createList(context, list, tr(T.dealtype), sorgu.dealTypeId,
        (v) => setState(() => sorgu.dealTypeId = v));
  }

  _sorguType() {
    final list = List.generate(10, (int index) => index + 1).map((i) {
      return DropdownMenuItem(child: Text(getSorguType(i)), value: i);
    }).toList();
    return createList(context, list, tr(T.sorgutype), sorgu.sorguTypeId,
        (v) => setState(() => sorgu.sorguTypeId = v));
  }

  _budget() {
    return Row(
      children: [
        budget(
            controller: priceController,
            labelText: isSorgu(sorgu.dealTypeId) ? tr(T.budget) : tr(T.priceo),
            onSaved: (String value) {
              sorgu.price = int.tryParse(value.trim().replaceAll(',', ''));
            },
            dealTypeId: sorgu.dealTypeId
        ),
        currency(
            value: sorgu.currency,
            onChange: (v) => setState(() => sorgu.currency = v)),
      ],
    );
  }


  _rooms(context) {
    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(children: [
        Expanded(
          child: createList(
            context,
            genDropDownItems(min: 0, count: 20),
            isSorgu(sorgu.dealTypeId) ? tr(T.roomsmin): tr(T.rooms),
            sorgu.roomsMin,
            (v) => setState(() {
              sorgu.roomsMin = v;
              if (sorgu.roomsMax < v) sorgu.roomsMax = v;
            }),
          ),
        ),
        if (isSorgu(sorgu.dealTypeId))
          Expanded(
            child: createList(
              context,
              genDropDownItems(min: sorgu.roomsMin, count: 20),
              tr(T.roomsmax),
              sorgu.roomsMax,
              (v) => setState(() => sorgu.roomsMax = v),
            ),
          ),
      ]),
    );
  }

  _area(context) {
    final pad = 8.0;

    return Padding(
      padding: const EdgeInsets.all(8),
      child: Row(children: [
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: pad, right: pad, bottom: pad),
            child: TextFormField(
              controller: TextEditingController(text: '${sorgu.areaMin}'),
              keyboardType: TextInputType.number,
              inputFormatters: [NumericTextFormatter()],
              decoration: InputDecoration(
                  labelStyle: TextStyle(fontSize: 12),
                  labelText:
                      isSorgu(sorgu.dealTypeId) ? tr(T.areamin) : tr(T.area)),
              onChanged: (v) {
                var value = 0;
                try {
                  value = int.parse(v);
                } catch (e) {}
                sorgu.areaMin = value;
                sorgu.areaMax = sorgu.areaMax < value ? value : sorgu.areaMax;
              },
            ),
          ),
        ),
        if (isSorgu(sorgu.dealTypeId))
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: pad, right: pad, bottom: pad),
              child: TextFormField(
                controller: TextEditingController(text: '${sorgu.areaMax}'),
                keyboardType: TextInputType.number,
                inputFormatters: [NumericTextFormatter()],
                decoration: InputDecoration(
                  labelStyle: TextStyle(fontSize: 12),
                  labelText: tr(T.areamax),
                ),
                onChanged: (v) {
                  var value = 0;
                  try {
                    value = int.parse(v);
                  } catch (e) {}
                  sorgu.areaMax = value;
                },
              ),
            ),
          )
      ]),
    );
  }

  _repair() {
    return repair(
        context: context,
        value: sorgu.repair,
        onChanged: (v) => setState(() => sorgu.repair = v));
  }

  _kupchay() => boolItem(
      context: context,
      label: tr(T.cixarish),
      value: sorgu.kupchay,
      onChanged: (v) => setState(() => sorgu.kupchay = v));

  _gas() => boolItem(
      context: context,
      label: tr(T.gas),
      value: sorgu.gas,
      onChanged: (v) => setState(() => sorgu.gas = v));

  _countries(context) => countries(
      context: context,
      future: fCountries,
      value: sorgu.countryId,
      onChanged: (v) {
        bloc.getCities(v);

        setState(() {
          sorgu.countryId = v;
          sorgu.cityId = 0;
          sorgu.districtsIds = [];
          sorgu.villagesIds = [];
          sorgu.metrosIds = [];
        });
      });

  _cities(context) => cities(
        context: context,
        stream: bloc.citiesStream,
        onData: (data) {
          if (data.length > 0 && (sorgu.cityId == 0 || sorgu.cityId == null)) {
            // to set state here
            sorgu.cityId = data[0].id;
          }
        },
        value: sorgu.cityId,
        onChanged: (v) {
          bloc.getDistricts(v);
          bloc.getVillages(v);
          bloc.getMetros(v);
          setState(() {
            sorgu.cityId = v;
            sorgu.districtsIds = [];
            sorgu.villagesIds = [];
            sorgu.metrosIds = [];
          });
        },
      );

  var allDistricts = [];

  _districts() => MultiSelect(
        label: tr(T.districts),
        stream: bloc.districtsStream,
        initialValue: sorgu.districtsIds,
        onChanged: (v) {
          setState(() {
            sorgu.districtsIds = v;
          });
        },
        onSteamDone: (items) {
          allDistricts = items;
        },
        validator: (items) {
          final res = items.length == 0 && allDistricts.length != 0
              ? tr(T.districtsnotempty)
              : null;
          return res;
        },
      );

  _villages() => MultiSelect(
        stream: bloc.villagesStream,
        label: tr(T.villages),
        initialValue: sorgu.villagesIds,
        onChanged: (v) {
          setState(() {
            sorgu.villagesIds = v;
          });
        },
      );

  _metros() => MultiSelect(
        stream: bloc.metrosStream,
        label: tr(T.metros),
        initialValue: sorgu.metrosIds,
        onChanged: (v) {
          setState(() {
            sorgu.metrosIds = v;
          });
        },
      );

  _note() {
    return Padding(
      padding: const EdgeInsets.only(left: pad, right: pad, bottom: pad),
      child: TextFormField(
        controller: noteController,
        maxLines: 4,
        decoration: InputDecoration(labelText: tr(T.note)),
        onSaved: (v) {
          sorgu.note = v.trim();
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    print('B:_AddRequestsScreenState');
    return Scaffold(
      appBar: AppBar(
        title: Text(isUpdate ? '${tr(T.updaterequest)}' : tr(T.createrequest)),
        elevation: 0.5,
        actions: [
          FlatButton(
            child: Text(tr(T.reset), style: TextStyle(color: Colors.white)),
            onPressed: _onReset,
          ),
        ],
        leading: IconButton(
            icon: Icon(Icons.clear), onPressed: () => Navigator.pop(context)),
      ),
      body: GestureDetector(
        // TODO: hacky solution. Find better way of unfocusing textfield
        behavior: HitTestBehavior.opaque,
        onTap: () => FocusScope.of(context).requestFocus(FocusNode()),
        child: Builder(
          builder: (context) => Stack(
            children: [
              SingleChildScrollView(
                child: _filterItems(context),
              ),
              SafeArea(
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 4.0),
                    child: RaisedButton(
                        color: Colors.red,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 16.0, horizontal: 64),
                          child: Text(
                            isUpdate ? tr(T.update) : tr(T.create),
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        onPressed: () => _onSubmit(context)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
