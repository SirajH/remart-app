import 'dart:async';

import 'package:flutter/material.dart';
import 'package:remart/bloc/LoginBloc.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/bloc/MessageBloc.dart';
import 'package:remart/common/MessageBuilder.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/Validator.dart';
import 'package:remart/common/common.dart';
import 'package:remart/model/Msg.dart';
import 'package:remart/model/Status.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

import '../theme.dart';
import 'RegisterScreen.dart';

class LoginPage extends StatefulWidget {
  final String msisdn;

  LoginPage({this.msisdn = ''});

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    pd('B:LoginPage');
    return VccScaffold(
//        leading: Column(
//          mainAxisAlignment: MainAxisAlignment.end,
//          mainAxisSize: MainAxisSize.min,
//          children: [
//            Icon(Icons.arrow_back_ios)
//          ],
//        ),
      leading: Row(
        children: [
          IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () =>
                Navigator.pushNamedAndRemoveUntil(context, '/', (val) => false),
          ),
        ],
      ),
      child: LoginView(msisdn: widget.msisdn),
      localeSelected: localeSelected,
    );
  }

  localeSelected(locale) =>
      setState(() => TranslationService().changeLocale(locale));
}

class LoginView extends StatefulWidget {
  final String msisdn;

  LoginView({this.msisdn});

  @override
  LoginViewState createState() => LoginViewState();
}

class LoginViewState extends State<LoginView> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();

  LoginBloc _loginBloc = LoginBloc();

//  LogoutBloc _logoutBloc = LogoutBloc();
//  MessageBloc _msgBloc = MessageBloc();

  String _msisdn, _password;

//  var myController = TextEditingController(text: '+994');
  var msisdnCtrl = TextEditingController(text: '+994');
  var passwordCtrl = TextEditingController(text: '');

//  StreamSubscription subs, subs2;
  var hidePassword = true;
  bool rememberMe = false;

  @override
  void initState() {
    pd('IS:LoginViewState');

    if (widget.msisdn != null && widget.msisdn != '') {
      msisdnCtrl = TextEditingController(text: widget.msisdn);
    }
//    subs = _loginBloc.loginStatusStream.listen((Status status) async {
//      print('CCC:loginStatusStream $status');
//      if (status == Status.OK) {
//        await hideKeyboard(context);
//        _loginBloc.goToMain(context);
//      }
//    });

//    subs2 = _logoutBloc.errorStream.listen((String msg) {
//      print('CCC:errorStream $msg');
//
//      _msgBloc.showMsg(Msg(msg));
//      _loginBloc.addLoginStatus(Status.PENDING);
//    });

//    _loginBloc.rememberedCreds.then((creds) {
//      if (creds != null) {
    // TODO uncomment it if remember creds implemented
//        msisdnCtrl = TextEditingController(text: creds[Constants.MSISDN]);
//        passwordCtrl = TextEditingController(text: creds[Constants.PASSWORD]);
//        setState(() {});
//      }
//    });

    super.initState();
  }

  @override
  void dispose() {
    pd('D:LoginViewState');
    _loginBloc.dispose();
//    subs.cancel();
//    subs2.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RemartCard(
      child: Padding(
        padding: EdgeInsets.only(left: pad, right: pad, bottom: 10),
        child: Form(
          key: this._formKey,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 10),
              Text(tr(T.welcome), style: cardHeader),
//              Padding(
//                padding: const EdgeInsets.all(16.0),
//                child:
//                    Text(tr(T.loginwelcomedesc), textAlign: TextAlign.center),
//              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // TODO: return this if you want country code selectable
//                  Expanded(
//                    flex: 1,
//                    child: TextFormField(
//                      enabled: false,
//                      controller: myController,
//                      decoration: InputDecoration(labelText: ''),
//                    ),
//                  ),
//                  SizedBox(width: 8),
                  Expanded(
                    flex: 5,
                    child: TextFormField(
                      controller: msisdnCtrl,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: tr(T.yourmsisdn),
                        suffixIcon: Icon(Icons.phone),
                        helperText: '${tr(T.example)}: +994509998877',
                        alignLabelWithHint: true,
                      ),
                      validator: (value) {
                        return Validator.msisdn(value.trim());
                      },
                      onSaved: (String value) {
                        this._msisdn = value.trim();
                      },
                    ),
                  ),
                ],
              ),
              TextFormField(
                obscureText: hidePassword,
                controller: passwordCtrl,
                decoration: InputDecoration(
                    labelText: tr(T.password),
                    suffixIcon: IconButton(
                      icon: Icon(
                          hidePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Colors.grey),
                      onPressed: () {
                        setState(() => hidePassword = !hidePassword);
                      },
                    )),
                onSaved: (String value) => this._password = value.trim(),
              ),
//              Row(
//                mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                children: [
//                  Text(tr(T.rememberme)),
//                  Checkbox(
//                      onChanged: (val) {
//                        setState(() {
//                          rememberMe = val;
//                        });
//                      },
//                      value: rememberMe,
//                      checkColor: primaryColor, // color of tick Mark
//                      activeColor: Colors.white)
//                ],
//              ),
              SizedBox(height: pad),
              StreamBuilder(
                stream: _loginBloc.loginStatusStream,
                builder: (context, AsyncSnapshot snapshot) {
                  return RemartButton(
                      horizontalPad: 40,
                      text: tr(T.login),
                      status: snapshot.data,
                      onTap: () => _login(context));
                },
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
//                  Material(
//                    color: Colors.transparent,
//                    child: InkWell(
//                      child: Container(
//                        padding: EdgeInsets.symmetric(
//                            vertical: pad16, horizontal: pad8),
//                        child: Row(
//                          mainAxisAlignment: MainAxisAlignment.center,
//                          children: [
//                            Icon(Icons.input,
//                                color: Color(0xff6AA6FF), size: 16),
//                            SizedBox(width: 6),
//                            Text(
//                              tr(T.fastlogin),
//                              style: linkStyle,
//                            ),
//                          ],
//                        ),
//                      ),
//                      onTap: () {
//                        Navigator.push(
//                          context,
//                          MaterialPageRoute(
//                            builder: (context) =>
//                                FastLoginPage(msisdn: msisdnCtrl.text),
//                          ),
//                        );
//                      },
//                    ),
//                  ),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: pad16, horizontal: pad8),
                        child: Text(
                          tr(T.register),
                          style: linkStyle,
                        ),
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => RegisterScreen(),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
              MessageBuilder()
            ],
          ),
        ),
      ),
    );
  }

  _login(BuildContext context) async {
    if (this._formKey.currentState.validate()) {
      this._formKey.currentState.save();
      String res = await _loginBloc.login(_msisdn, _password);
      if (res == 'ok') {
        await hideKeyboard(context);
        Navigator.pushNamedAndRemoveUntil(context, '/', (val) => false);
      } else {
        Scaffold.of(context).showSnackBar(SnackBar(content: Text(tr(res))));
        _loginBloc.addLoginStatus(Status.PENDING);
      }
//      if (res == 'ok' && rememberMe) _loginBloc.rememberMe(_msisdn, _password);
    }
  }
}
