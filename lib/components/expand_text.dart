import 'package:flutter/material.dart';
import 'package:remart/common/T.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/theme/remart_theme.dart';

// ignore: must_be_immutable
class ExpandText extends StatefulWidget {
  final String text;
  final charCount;
  final TextStyle style;
  var isOpen = false;

  ExpandText(this.text,
      {this.charCount = 200, this.style = const TextStyle(height: 1.2)});

  @override
  ExpandTextState createState() {
    return new ExpandTextState();
  }
}

class ExpandTextState extends State<ExpandText> {
  @override
  Widget build(BuildContext context) {
    return widget.text != ''
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                widget.isOpen
                    ? widget.text
                    : widget.text.substring(
                        0,
                        widget.text.length <= widget.charCount
                            ? widget.text.length
                            : widget.charCount),
                style: widget.style,
              ),
              widget.text.length <= widget.charCount
                  ? SizedBox()
                  : GestureDetector(
                      behavior: HitTestBehavior.opaque,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 5, right: 20, bottom: 10),
                        child: Text(
                          widget.isOpen ? tr(T.less) : tr(T.more),
                          style: TextStyle(color: linkColor),
                        ),
                      ),
                      onTap: () {
                        setState(() {
                          widget.isOpen = !widget.isOpen;
                        });
                      },
                    )
            ],
          )
        : SizedBox();
  }
}
