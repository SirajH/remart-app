import 'package:flutter/material.dart';

class ButtonWithLabel extends StatelessWidget {
  final String title;
  final IconData icon;
  final onTap;
  final onReturn;
  final Widget page;
  final int count;

  ButtonWithLabel(
    this.title,
    this.icon, {
    Key key,
    this.onTap,
    this.onReturn,
    this.page,
    this.count = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        FlatButton(
          child: Row(
            children: [
              Icon(icon, color: Colors.white, size: 18),
              SizedBox(width: 4),
              Text(title, style: TextStyle(color: Colors.white)),
            ],
          ),
          onPressed: onTap ??
              () {
                Navigator.push(
                        context, MaterialPageRoute(builder: (context) => page))
                    .then((val) => onReturn(val));
              },
        ),
        if (count > 0)
          Positioned(
              right: 0,
              child: Material(
                borderRadius: BorderRadius.circular(20),
                elevation: 2,
                child: Container(
                  width: 20,
                  height: 20,
                  decoration: BoxDecoration(
                      color: Colors.yellow,
                      borderRadius: BorderRadius.circular(20)),
                  child: Center(
                    child: Text(
                      '$count',
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ),
              ))
      ],
    );
  }
}
