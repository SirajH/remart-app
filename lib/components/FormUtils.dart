import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/Validator.dart';
import 'package:remart/components/MultiSelectDialog.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

// TODO: use it in FilterView
createList(BuildContext context, List<DropdownMenuItem<int>> list, String label,
    int value, onChanged) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 16),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(color: Theme.of(context).accentColor),
        ),
        DropdownButton(
          isExpanded: true,
          value: value,
          items: list,
          onChanged: (v) => onChanged(v),
        ),
      ],
    ),
  );
}

budget(
    {TextEditingController controller,
    onSaved,
    bool required = true,
    labelText = '',
    dealTypeId: 1}) {
  const pad = 16.0;
  return Expanded(
    flex: 4,
    child: Padding(
      padding: const EdgeInsets.only(left: pad, right: pad, bottom: pad),
      child: TextFormField(
        controller: controller,
        keyboardType: TextInputType.number,
        inputFormatters: [NumericTextFormatter()],
        decoration: InputDecoration(labelText: labelText),
        validator: (value) {
          value = value ?? '';
          return Validator.validateBudget(
              value.trim().replaceAll(',', ''), required);
        },
        onSaved: onSaved,
      ),
    ),
  );
}

currency({value, onChange, bool withEmptyValue = false}) {
  final items = currencies.keys
      .map((k) => DropdownMenuItem(child: Text('  ${currencies[k]}'), value: k))
      .toList();
  if (withEmptyValue) items.insert(0, egal);
  return Expanded(
    flex: 2,
    child: Padding(
      padding: const EdgeInsets.only(right: 16, top: 12),
      child: DropdownButton(
        isExpanded: true,
        value: value,
        items: items,
        onChanged: onChange,
      ),
    ),
  );
}

repair({context, value, onChanged}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Text(
          tr(T.reapirstatus),
          style: TextStyle(color: Theme.of(context).accentColor),
        ),
        SizedBox(height: 12),
        Container(
          width: double.infinity,
          child: CupertinoSegmentedControl(
              groupValue: value,
              children: {
                2: Text(tr(T.temirsiz)),
                0: Text(tr(T.ferqetmez)),
                1: Text(tr(T.temirli)),
              },
              onValueChanged: onChanged),
        ),
      ],
    ),
  );
}

boolItem({context, label, value, onChanged}) {
  return Padding(
    padding: EdgeInsets.only(left: 16, right: 8),
    child: Row(
      children: [
        Text(label, style: TextStyle(color: Theme.of(context).accentColor)),
        Expanded(child: SizedBox()),
        Switch(
          activeColor: Colors.green,
          value: value,
          onChanged: onChanged,
        )
      ],
    ),
  );
}

countries({context, future, value, onChanged}) {
  return FutureBuilder<List<Country>>(
      future: future,
      builder: (context, snap) {
//          print('>>> fCountries snap: $snap');
        if (snap.connectionState == ConnectionState.waiting)
          return Text(tr(T.yuklenir));
        if (snap.hasError) return Text(tr(T.erroroccured));
        if (snap.hasData) {
          return Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  tr(T.countries),
                  style: TextStyle(color: Theme.of(context).accentColor),
                ),
                DropdownButton(
                  isExpanded: true,
                  value: value,
                  items: snap.data
                      .map((c) =>
                          DropdownMenuItem(child: Text(c.value), value: c.id))
                      .toList(),
                  onChanged: onChanged,
                ),
              ],
            ),
          );
        }
      });
}

cities({context, stream, value, onChanged, onData}) {
  return StreamBuilder<List<City>>(
      stream: stream,
      builder: (context, snap) {
//          print('>>> fCities snap: $snap');
        if (snap.connectionState == ConnectionState.waiting)
          return Text(tr(T.yuklenir));
        if (snap.hasError) return Text(tr(T.erroroccured));
        if (snap.hasData) {
          // DropdownButton throws error when filter.cityId == 0
          // and snap.data has values where id == 0 not exits
          onData(snap.data);
          // The change to value on the parent will not affect to following value
          // So we set value here
          if (snap.data.length > 0 && (value == 0 || value == null)) {
            value = snap.data[0].id;
          }

          return snap.data.length > 0
              ? Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        tr(T.cities),
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                      DropdownButton(
                        isExpanded: true,
                        value: value,
                        items: snap.data
                            .map((c) => DropdownMenuItem(
                                child: Text(c.value), value: c.id))
                            .toList(),
                        onChanged: onChanged,
                      ),
                    ],
                  ),
                )
              : SizedBox();
        }
      });
}

class NumericTextFormatter extends TextInputFormatter {
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    if (newValue.text.length == 0) {
      return newValue.copyWith(text: '');
    } else if (newValue.text.compareTo(oldValue.text) != 0) {
      int selectionIndexFromTheRight =
          newValue.text.length - newValue.selection.end;
      final f = NumberFormat("#,###");
      int num = int.parse(newValue.text.replaceAll(f.symbols.GROUP_SEP, ''));
      final newString = f.format(num);
      return TextEditingValue(
        text: newString,
        selection: TextSelection.collapsed(
            offset: newString.length - selectionIndexFromTheRight),
      );
    } else {
      return newValue;
    }
  }
}

class MultiSelect<K> extends FormField<List<int>> {
  final Stream stream;
  final String label;
  final List<int> initialValue;
  final Function onChanged;
  final Function onSteamDone;

  MultiSelect({
    this.onSteamDone,
    this.onChanged,
    this.stream,
    this.label,
    this.initialValue,
    FormFieldSetter<List<int>> onSaved,
    FormFieldValidator<List<int>> validator,
  }) : super(
            initialValue: initialValue,
            onSaved: onSaved,
            validator: validator,
            builder: (FormFieldState<List<int>> field) {
              if (initialValue.length != field.value.length) {
                field.reset();
              }
              return StreamBuilder<List<District>>(
                  stream: stream,
                  builder: (context, snap) {
                    if (snap.connectionState == ConnectionState.waiting)
                      return Text(tr(T.yuklenir));
                    if (snap.hasError) return Text(tr(T.erroroccured));
                    if (snap.hasData) {
                      if (onSteamDone != null) onSteamDone(snap.data);
                      if (snap.data.length > 0) {
                        _MultiSelectState f = field;
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            _buildDialogChips(
                              context,
                              label,
                              field.value,
                              Map.fromIterable(snap.data,
                                  key: (v) => v.id, value: (v) => v.value),
                              (v) {
                                // Set current value
                                field.didChange(v);
                                if (onChanged != null) onChanged(v);
                              },
                            ),
//                            Text('Error Text:  ${field.errorText}'),
                            if (field.errorText != null)
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                child: Text(field.errorText,
                                    style: TextStyle(
                                        color: Colors.red[800], fontSize: 12)),
                              ),
                            divider
                          ],
                        );
                      } else {
                        return SizedBox();
                      }
                    }
                  });
            });

  static _buildDialogChips(context, String title, List<int> items,
      Map<int, String> itemsAll, onChanged) {
    List<MultiSelectDialogItem<int>> multiIitems = itemsAll.keys
        .map((k) => MultiSelectDialogItem(k, itemsAll[k]))
        .toList();

    return Padding(
      padding: EdgeInsets.only(left: 16, right: 8),
      child: Column(
        children: [
          Row(
            children: [
              Text(title),
              Expanded(child: SizedBox()),
              FlatButton(
                child: Text(tr(T.add),
                    style: TextStyle(color: Theme.of(context).accentColor)),
                onPressed: () async {
                  final result = await showDialog(
                      context: context,
                      builder: (context) {
                        return MultiSelectDialog(
                          title: title,
                          items: multiIitems,
                          initialSelectedValues: items,
                        );
                      });
                  if (result == null) return;
                  onChanged(result);
                },
              )
            ],
          ),
          Container(
            width: double.infinity,
            child: Wrap(
              spacing: 2,
              children: _getChipValues(items, itemsAll),
            ),
          )
        ],
      ),
    );
  }

  static List<Widget> _getChipValues(List<int> items, allItems) {
    final count = 3;
    List<int> res = items;
    if (items.length > count) res = items.take(3).toList();
    List<Widget> result = res
        .map((k) => Chip(
              label: Text(
                allItems[k],
                style: TextStyle(fontSize: 12),
              ),
            ))
        .toList();
    if (items.length > count)
      result.add(Chip(
          label: Text(
        '+ ${items.length - count} ${tr(T.more)}',
        style: TextStyle(fontSize: 12),
      )));

    return result;
  }

  @override
  _MultiSelectState createState() => new _MultiSelectState();
}

class _MultiSelectState extends FormFieldState<List<int>> {}

List<DropdownMenuItem<int>> genDropDownItems(
    {int min = 0, int count = 20, int step = 1}) {
  return List<int>.generate(count, (int index) => (index * step + min))
      .map((i) {
    if (i == 0) return egal;
    return DropdownMenuItem(child: Text('$i'), value: i);
  }).toList();
}
