import 'package:flutter/material.dart';
import 'package:remart/theme/remart_theme.dart';

class InfoItemRow extends StatelessWidget {
  final String title;
  final String subtitle;
  final Widget action;

  InfoItemRow({this.subtitle, this.title, Key key, this.action})
      : super(key: key);

  final header = TextStyle(color: lightGrayIconColor, fontSize: 13);
  final text = TextStyle(
      color: primaryBlue,
      fontWeight: FontWeight.w600,
      height: 1.5,
      fontSize: 14);

  @override
  Widget build(BuildContext context) {
    return subtitle != null && subtitle != ''
        ? ListTile(
            dense: true,
            title: Text(title ?? '', style: header),
            subtitle: Text(subtitle, style: text),
            trailing: action,
          )
        : SizedBox();
  }
}
