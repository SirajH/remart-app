import 'package:flutter/material.dart';

class StatusLabel extends StatelessWidget {
  final String text;
  final Color color;
  final Color textColor;
  final onTap;

  const StatusLabel(
      {Key key,
      this.text,
      this.color = const Color(0xffeeeeee),
      this.textColor = Colors.black54,
      this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(4)), color: color),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 4),
        child: Text(
          text.toUpperCase(),
          style: TextStyle(color: textColor, fontSize: 10),
        ),
      ),
    );
  }
}
