import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:remart/common/T.dart';
import 'package:remart/components/StatusIcon.dart';
import 'package:remart/model/RequestStatus.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';
import 'package:remart/theme/remart_theme.dart';
import 'package:share/share.dart';

class RequestItem extends StatefulWidget {
  final Sorgu request;
  final onTap;
  final onEdit;
  final bool isMy;
  final bloc;

  const RequestItem(
      {Key key,
      this.request,
      this.bloc,
      this.onTap,
      this.onEdit,
      this.isMy = false})
      : super(key: key);

  @override
  _RequestItemState createState() => _RequestItemState();
}

class _RequestItemState extends State<RequestItem> {
  var shareText = '';
  final ts = TranslationService();

  void _menuItemSelected(String val, context) {
    switch (val) {
      case C.share:
        Share.share(shareText);
        break;
//      case C.taketowork:
//        _takeToWork(context);
        break;
      case C.edit:
        _edit();
        break;
      case C.activate:
        _changeStatus(context, RequestStatus.active);
        break;
      case C.deactivate:
        _changeStatus(context, RequestStatus.deactive);
        break;
      case C.delete:
        _changeStatus(context, RequestStatus.deleted);
        break;
    }
  }

  _changeStatus(BuildContext context, int status) async {
    final res = await showPrompt(context, tr(T.alert), tr(T.areyousure));
    if (res ?? false) {
      widget.onEdit(widget.request, status: status);
    }
  }

  _edit() => widget.onEdit(widget.request);

//  _takeToWork(context) async {
//    bool result = await showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return AlertDialog(
//          title: Text("Alert"),
//          content: TakeToWorkWidget(),
//          actions: [
//            FlatButton(
//              child: Text("ADD"),
//              onPressed: () async {
//              },
//            ),
//            FlatButton(
//              child: Text("CANCEL"),
//              onPressed: () => Navigator.pop(context, false),
//            ),
//          ],
//        );
//      },
//    );
//
//    if (result)
//      Scaffold.of(context)
//          .showSnackBar(SnackBar(content: Text('Add to in progress')));
//  }

  @override
  Widget build(BuildContext context) {
    return Material(
      elevation: 1.5,
      shadowColor: Colors.grey[100],
      color: Colors.white,
      child: InkWell(
        onTap: widget.onTap,
        child: IntrinsicHeight(
          child: Stack(
            children: [
              Align(
                alignment: Alignment.centerLeft,
                child: Container(
                  color: isSorgu(widget.request.dealTypeId)
                      ? Colors.redAccent
                      : Colors.green[400],
                  width: 6,
                ),
              ),

//            buildPopupMenuItem(context), // TODO: recover it to add more_vert i.e. share functionality
              Container(
                padding: EdgeInsets.all(20),
                child: Stack(
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
//                  Text('ID: ${widget.request.id}'),
                        _buildHeader(),
                        SizedBox(height: 6),
                        Text(getSorguType(widget.request.sorguTypeId),
                            style: TextStyle(
                                fontSize: 13,
                                fontWeight: FontWeight.w600,
                                color: typeColors[widget.request.sorguTypeId])),
                        SizedBox(height: 5),
                        Text(_address,
                            style: Theme.of(context).textTheme.body1),
                        SizedBox(height: 2),
                        _buildMainValues(context),
                        SizedBox(height: 10),
                        _buildCreator(context),
                      ],
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  String get _address {
    final r = widget.request;
    String res = '${r.city}';
    if (r.country.id != SessionService.countryId) {
      res += ' (${r.country})';
    }
    res += ', ${r.district}${r.district != '' ? ',' : ''} ';
    return res;
  }

  Widget _buildHeader() {
    return Row(
      children: [
        Text(
          getDealType(widget.request.dealTypeId),
          style: TextStyle(
              fontSize: 12, fontWeight: FontWeight.w600, color: Colors.black54),
        ),
        SizedBox(width: 8),
        _buildStatusLabel()
      ],
    );
  }

  _buildStatusLabel() {
    if (!widget.isMy) return SizedBox();
    return widget.request.status == RequestStatus.deactive
        ? StatusLabel(text: RequestStatus.value(widget.request.status))
        : StatusLabel(
            text: RequestStatus.value(widget.request.status),
            textColor: Colors.white,
            color: Colors.green,
          );
  }

  RichText _buildMainValues(BuildContext context) {
    final data = widget.request.toReadable();

    final list = <TextSpan>[
      if (isSorgu(widget.request.dealTypeId)) _comma(data[C.price]),
      if (!isSorgu(widget.request.dealTypeId)) _comma(data[T.priceo]),
      _comma(data[C.room]),
      _comma(data[C.area]),
      TextSpan(
          text:
              widget.request.kupchay ? '${tr(T.cixarish).toLowerCase()}, ' : '',
          style: bold),
      TextSpan(
          text: widget.request.gas ? '${tr(T.gas).toLowerCase()}, ' : '',
          style: bold),
    ];

    if (widget.request.repair == 1) {
      list.add(TextSpan(
          text: '${tr(T.withrepair).toString().toLowerCase()} ', style: bold));
    }
    if (widget.request.repair == 2) {
      list.add(TextSpan(
          text: '${tr(T.withoutrepair).toString().toLowerCase()} ',
          style: bold));
    }

    final rt = RichText(
      text: TextSpan(
        style: Theme.of(context).textTheme.body1,
        children: list,
      ),
    );

//    _generateShareText(rt); // TODO: recover it if you wanna share

    return rt;
  }

  _generateShareText(RichText rt) {
    shareText =
        """${isSorgu(widget.request.dealTypeId) ? tr(T.sifarish) : tr(T.offer)}: ${getSorguType(widget.request.sorguTypeId)} - ${getDealType(widget.request.dealTypeId)}
$_address
${Util.getRichTextValue(rt)}
${widget.request.realtor.fullName}
${tr(T.phoneshort)}: ${widget.request.realtor.phone}
${tr(T.whatsapp)}: ${widget.request.realtor.whatsapp}""";
//    print(shareText);
//    print('-------------------------');
  }

  _comma(TextSpan val) => val != null && val.text != ''
      ? TextSpan(children: [val, TextSpan(text: ', ')])
      : TextSpan(text: '');

  Widget _buildCreator(context) {
    return Row(
      children: [
        CircleAvatar(
          backgroundImage: NetworkImage(widget.request.realtor.avatar),
          radius: 12.0,
        ),
        SizedBox(width: 8),
        Text(
          widget.request.realtor.fullName,
          style: TextStyle(fontSize: 13, color: Colors.black45),
        ),
        Expanded(child: SizedBox()),
        Text(
          getRelativeDate(widget.request.updated),
          style: Theme.of(context).textTheme.caption,
        )
      ],
    );
  }

  Align buildPopupMenuItem(BuildContext context) {
    final items = [
      PopupMenuItem(value: C.share, child: Text(tr(T.share))),
//      PopupMenuItem(value: C.taketowork, child: Text('Bзять в работу')),
    ];

    final myItems = [
      PopupMenuItem(value: C.edit, child: Text(tr(T.edit))),
      PopupMenuItem(value: C.delete, child: Text(tr(T.delete))),
      PopupMenuItem(value: C.share, child: Text(tr(T.share)))
    ];

    switch (widget.request.status) {
      case RequestStatus.active:
        myItems.add(
            PopupMenuItem(value: C.deactivate, child: Text(tr(T.deactivate))));
        break;
      case RequestStatus.deactive:
        myItems
            .add(PopupMenuItem(value: C.activate, child: Text(tr(T.activate))));
        break;
    }

    return Align(
      alignment: Alignment.topRight,
      child: Padding(
        padding: const EdgeInsets.only(top: 4, right: 4),
        child: PopupMenuButton(
          itemBuilder: (context) {
            return widget.isMy ? myItems : items;
          },
          onSelected: (val) => _menuItemSelected(val, context),
          icon: Icon(Icons.more_vert, color: Colors.black54),
        ),
      ),
    );
  }
}
