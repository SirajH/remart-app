import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

class Whatsapp extends StatelessWidget {
  final String phone;
  final String text;

  const Whatsapp({Key key, this.phone, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(FontAwesomeIcons.whatsapp),
//      color: iconColor,
      onPressed: () {
        if (phone != null && phone.isNotEmpty)
          launch("whatsapp://send?phone=$phone&text=$text");
      },
    );
  }
}
