import 'package:flutter/material.dart';
import 'package:remart/theme/remart_theme.dart';

class LinkRow extends StatelessWidget {
  final String title;
  final int count;
  final IconData icon;
  final onTap;

  const LinkRow({Key key, this.title, this.count, this.icon, this.onTap})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTap,
      title: Text(title,
          style: TextStyle(color: primaryBlue, fontWeight: FontWeight.w600)),
      leading: Icon(icon, color: lightGrayIconColor),
      trailing: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(count.toString(), style: TextStyle(color: Colors.blue)),
          Icon(Icons.chevron_right, color: lightGrayIconColor)
        ],
      ),
    );
  }
}
