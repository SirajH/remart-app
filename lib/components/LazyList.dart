import 'package:flutter/material.dart';
import 'package:remart/model/Constants.dart';

class LazyList extends StatelessWidget {
  final List data;
  final Widget Function(int index) builder;
  final onMore;
  final hasMore;

  const LazyList(
      {Key key, this.data, this.builder, this.onMore, this.hasMore = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MediaQuery.removePadding(
      context: context,
      removeTop: true,
      child: ListView.separated(
          itemCount: data.length,
          separatorBuilder: (ctx, i) => SizedBox(height: 8),
          itemBuilder: (context, i) {
            if (hasMore &&
                data.length >= Constants.limit &&
                i == data.length - 1) {
              onMore();
              return Column(
                children: [
                  builder(i),
                  Padding(
                    padding: const EdgeInsets.only(top: 16, bottom: 64),
                    child: Align(child: CircularProgressIndicator()),
                  ),
                ],
              );
            }

            return builder(i);
          }),
    );
  }
}
