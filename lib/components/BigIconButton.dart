import 'package:flutter/material.dart';

class BigIconButton extends StatelessWidget {
  final IconData icon;
  final double iconSize;
  final String text;
  final onTap;

  const BigIconButton(
      {Key key, this.icon, this.text, this.onTap, this.iconSize = 28})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4, horizontal: 8),
        child: Column(
          children: [
            SizedBox(
              height: 30,
              child: Icon(
                icon,
                size: iconSize,
                color: Colors.black54,
              ),
            ),
            Text(
              text,
              style: TextStyle(fontSize: 11),
            ),
          ],
        ),
      ),
      onTap: onTap,
    );
  }
}
