import 'package:flutter/material.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/User.dart';
import 'package:remart/screen/MyProfileScreen.dart';
import 'package:remart/screen/ProfileScreen.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/Util.dart';

class ProfileRow extends StatelessWidget {
  final Realtor user;
  final List<Widget> actions;
  final List<Widget> statuses;
  final bool clickable;

  ProfileRow(
      {this.user,
      this.actions = const [],
      this.statuses = const [],
      this.clickable = true});

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: clickable ? () => _goToProfile(context) : null,
      leading: InkWell(
        onTap: () {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                contentPadding: EdgeInsets.all(0),
                content: FittedBox(
                  child: Image.network(user.avatar),
                ),
              );
            },
          );
        },
        child: CircleAvatar(
          backgroundImage: NetworkImage(user.avatar),
        ),
      ),
      title: statuses.length == 0
          ? Container(
              child: Text(user.fullName, overflow: TextOverflow.fade, softWrap: false,),
            )
          : Row(
              children: [
                Text(
                  user.fullName,
                  style: TextStyle(fontSize: 15),
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(width: 8),
                Row(children: statuses)
              ],
            ),
      subtitle: user.company != null
          ? Container(
              width: 20,
              child: Text(
                user.company,
                overflow: TextOverflow.fade,
                style: TextStyle(color: Colors.black38, fontSize: 13),
              ),
            )
          : null,
      trailing: Row(children: actions, mainAxisSize: MainAxisSize.min),
    );
  }

  _goToProfile(BuildContext context) {
    if (!isMe(user.id)) {
      Navigator.of(context).push(
          MaterialPageRoute(builder: (context) => ProfileScreen(user: user)));
    }
  }
}
