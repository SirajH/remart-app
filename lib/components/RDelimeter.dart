import 'package:flutter/material.dart';
import 'package:remart/theme/remart_theme.dart';

class RDelimeter extends StatelessWidget {
  final double height;

  const RDelimeter({Key key, this.height = 10}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: Color(0x103F51B5),
      height: height,
    );
  }
}
