import 'package:flutter/material.dart';

class MyCheckBox extends StatefulWidget {
  final String textActive;
  final String textDeac;
  final Color color;
  final bool value;
  final onTap;

  const MyCheckBox(
      {Key key,
      this.textActive,
      this.textDeac,
      this.color,
      this.value = false,
      this.onTap})
      : super(key: key);

  @override
  _MyCheckBoxState createState() => _MyCheckBoxState(value);
}

class _MyCheckBoxState extends State<MyCheckBox> {
  bool value;

  _MyCheckBoxState(this.value);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: value ? Colors.white : widget.color,
      borderRadius: BorderRadius.all(Radius.circular(4)),
      child: InkWell(
        onTap: () {
          setState(() => value = !value);
          widget.onTap(value);
        },
        child: Container(
            decoration: BoxDecoration(
                border:
                    Border.all(color: value ? Colors.grey[300] : widget.color),
                borderRadius: BorderRadius.all(Radius.circular(4))),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 4),
              child: Text(
                value ? widget.textActive : widget.textDeac,
                style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                    color: value ? Colors.black : Colors.white),
              ),
            )),
      ),
    );
  }
}
