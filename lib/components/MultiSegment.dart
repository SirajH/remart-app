import 'package:flutter/material.dart';

class MultiSegment extends StatefulWidget {
  final Map<dynamic, String> values;

  const MultiSegment({Key key, this.values}) : super(key: key);

  @override
  _MultiSegmentState createState() => new _MultiSegmentState();
}

class _MultiSegmentState extends State<MultiSegment> {
  List<dynamic> active = [];

  _buildSegment(bool i, key) {
    bool isFirst = widget.values.keys.first == key;
    bool isLast = widget.values.keys.last == key;
    return Container(
      width: 50,
      padding: EdgeInsets.symmetric(vertical: 4),
      decoration: BoxDecoration(
        color: i ? Theme.of(context).accentColor : Colors.white,
        borderRadius: _buildBorderRadius(isFirst, isLast),
        border: Border(
          top: BorderSide(color: Theme.of(context).accentColor),
          right: BorderSide(color: Theme.of(context).accentColor),
          bottom: BorderSide(color: Theme.of(context).accentColor),
          left: BorderSide(color: Theme.of(context).accentColor),
        ),
      ),
      child: Center(
        child: Text(
          widget.values[key],
          style: TextStyle(
              color: i ? Colors.white : Theme.of(context).accentColor),
        ),
      ),
    );
  }

  _buildValues() {
    return widget.values.keys.map((key) {
      return InkWell(
        child: _buildSegment(active.contains(key), key),
        onTap: () {
          if (active.contains(key)) {
            active.remove(key);
          } else {
            active.add(key);
          }
          setState(() {});
        },
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: _buildValues(),
      ),
    );
  }

  _buildBorderRadius(bool isFirst, bool isLast) {
    final radius = Radius.circular(4);
    if (isFirst) return BorderRadius.only(topLeft: radius, bottomLeft: radius);
    if (isLast) return BorderRadius.only(topRight: radius, bottomRight: radius);
    return BorderRadius.only();
  }
}