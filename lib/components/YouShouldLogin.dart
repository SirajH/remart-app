import 'package:flutter/material.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/common.dart';
import 'package:remart/service/TranslationService.dart';

class YouShouldLogin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(tr(T.logintosee)),
          SizedBox(height: 16),
          Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              RemartButton(
                primary: false,
                text: tr(T.login),
                onTap: () => LogoutBloc().logout(),
                horizontalPad: 30,
              )
            ],
          )
        ],
      ),
    );
  }
}
