import 'package:flutter/material.dart';

class RFab extends StatelessWidget {
  final String title;
  final IconData icon;
  final onTap;
  final onReturn;
  final Widget page;
  final int count;

  RFab(
    this.title,
    this.icon, {
    Key key,
    this.onTap,
    this.onReturn,
    this.page,
    this.count = 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Material(
          elevation: 6,
          borderRadius: BorderRadius.all(Radius.circular(30)),
          color: Theme.of(context).accentColor,
          child: InkWell(
            borderRadius: BorderRadius.all(Radius.circular(30)),
            onTap: onTap ??
                () {
                  Navigator.push(context,
                          MaterialPageRoute(builder: (context) => page))
                      .then((val) => onReturn(val));
                },
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.all(Radius.circular(30))),
              padding: EdgeInsets.fromLTRB(16.0, 12, 16.0, 12),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Icon(
                    icon,
                    color: Colors.white,
                    size: 20,
                  ),
                  SizedBox(width: 4),
                  Text(
                    title,
                    style: TextStyle(color: Colors.white),
                  )
                ],
              ),
            ),
          ),
        ),
        if (count > 0)
          Positioned(
            right: 0,
            child: Material(
              borderRadius: BorderRadius.circular(20),
              elevation: 2,
              child: Container(
                width: 20,
                height: 20,
                decoration: BoxDecoration(
                    color: Colors.yellow,
                    borderRadius: BorderRadius.circular(20)),
                child: Center(
                  child: Text(
                    '$count',
                    style: TextStyle(fontSize: 11),
                  ),
                ),
              ),
            ),
          )
      ],
    );
  }
}
