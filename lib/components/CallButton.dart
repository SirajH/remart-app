import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class CallButton extends StatelessWidget {
  final String phone;

  const CallButton({Key key, this.phone}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
        icon: Icon(Icons.phone),
        onPressed: () {
          if (phone != null && phone.isNotEmpty) launch("tel://$phone");
        });
  }
}
