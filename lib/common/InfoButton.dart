import 'package:flutter/material.dart';
import 'package:remart/service/TranslationService.dart';

import 'T.dart';

class InfoButton extends StatelessWidget {
  final String text;
  final double padding;
  static const double defaultPadding = 10;

  const InfoButton({Key key, @required this.text, this.padding = defaultPadding}) : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        child: Container(
          padding: EdgeInsets.all(padding),
          child: Icon(
            Icons.info_outline,
            color: Colors.blue,
            size: 20,
          ),
        ),
        onTap: () {
          showDialog(
              context: context,
              builder: (conte) => AlertDialog(
                  title: Text(tr(T.prompt)),
                  content: SingleChildScrollView(
                    child: Column(mainAxisSize: MainAxisSize.min, children: [
                      Text(text),
                    ]),
                  )));
        },
      ),
    );
  }
}
