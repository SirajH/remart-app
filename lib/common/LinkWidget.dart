import 'package:flutter/material.dart';

class LinkWidget extends StatelessWidget {
  final text;
  final onTap;

  const LinkWidget(this.text, {Key key, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: onTap,
        child: Row(children: [
          Text(
            text,
            style: TextStyle(color: Colors.blueAccent, fontSize: 13),
          ),
          Icon(Icons.chevron_right, color: Colors.blueAccent)
        ]),
      ),
    );
  }
}
