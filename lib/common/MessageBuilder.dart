import 'package:flutter/material.dart';
import 'package:remart/bloc/MessageBloc.dart';
import 'package:remart/service/Util.dart';

class MessageBuilder extends StatelessWidget {
  final bloc = MessageBloc();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        StreamBuilder(
          stream: bloc.messageStream,
          builder: (contex, AsyncSnapshot<String> snapshot) {
            if (snapshot.connectionState == ConnectionState.active &&
                snapshot.hasData &&
                snapshot.data != null) {
              Future.delayed(Duration(milliseconds: 100), () {
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text(snapshot.data)));
              });
              bloc.addMessageSubject(null);
            }

            return SizedBox();
          },
        ),
//        StreamBuilder(
//          stream: bloc.dialogStream,
//          builder: (contex, AsyncSnapshot snapshot) {
//            if (snapshot.connectionState == ConnectionState.active &&
//                snapshot.hasData &&
//                snapshot.data != null) {
//              Future.delayed(Duration(milliseconds: 200), () async {
//                MessageBody body = snapshot.data;
//                print('%^%^%^%^%^% ${snapshot.data}');
//                final res = await showPrompt(contex, body.title, body.content, barrierDismissible: false);
//                if (res) body.onSuccess();
//
//              });
//              bloc.addDialogSubject(null);
//            }
//
//            return SizedBox();
//          },
//        ),
      ],
    );
  }
}
