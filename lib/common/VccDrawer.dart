import 'package:flutter/material.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/model/Navs.dart';
import 'package:remart/screen/CoreView.dart';
import 'package:remart/service/HttpService.dart';
import 'package:remart/service/SessionService.dart';
import 'package:remart/service/TranslationService.dart';

import '../theme.dart';
import 'T.dart';
import 'common.dart';

class VccDrawer extends StatelessWidget {
  getListTile(title, icon, index, context, NavBloc navBloc) {
    return ListTile(
      dense: true,
      selected: navBloc.currRoute.index == index,
      leading: Icon(icon),
      title: Text(title),
      onTap: () {
        if (index == Navs.logout) {
          _logout(context);
          return;
        }
        navBloc.changeRoute(NavData(index));
        Navigator.pop(context);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final navBloc = NavProviderIWidget.of(context).navBloc;
    return Drawer(
        child: ListView(
      padding: EdgeInsets.zero,
      children: [
        _getDrawerHeader(context),
        getListTile(
            tr(T.requests), Icons.list, Navs.requests, context, navBloc),
        Divider(),
        getListTile(tr(T.myrequests), Icons.assignment, Navs.myrequests,
            context, navBloc),
        Divider(),
        getListTile(tr(T.myprofile), Icons.account_circle, Navs.myprofile,
            context, navBloc),
        Divider(),
        getListTile(
            tr(T.realtors), Icons.people, Navs.realtors, context, navBloc),
        Divider(),
        getListTile(
            tr(T.settings), Icons.settings, Navs.settings, context, navBloc),
        Divider(),
        if (SessionService.isLogged)
          getListTile(
              tr(T.logout), Icons.exit_to_app, Navs.logout, context, navBloc),
      ],
    ));
  }

  _getDrawerHeader(BuildContext context) {
    if (SessionService.isLogged)
      return DrawerHeader(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 16),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.only(left: 8.0),
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(SessionService.user.avatar),
                    radius: 24.0,
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    SizedBox(width: 8),
                    Text('${SessionService.user.fullName}',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w600)),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: Text('${SessionService.login}',
                      style: TextStyle(
                          fontSize: 16, color: Colors.white.withOpacity(0.7))),
                )
              ],
            ),
            InkWell(
                child: Padding(
                  padding: EdgeInsets.only(right: 12.0),
                  child: Icon(Icons.exit_to_app, color: Colors.white),
                ),
                onTap: () => _logout(context)),
          ],
        ),
        decoration: BoxDecoration(
          color: primaryColor,
        ),
      );
    else
      return DrawerHeader(
        padding: EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundImage: NetworkImage('https://app.remart.az/avatar.png'),
                ),
                SizedBox(width: 4),
                FlatButton(
                  child: Text(tr(T.login), style: TextStyle(color: Colors.white, fontSize: 16)),
                  onPressed: () => LogoutBloc().logout(),
                )
              ],
            ),
          ],
        ),
        decoration: BoxDecoration(
          color: primaryColor,
        ),
      );
  }

  _logout(context) async {
    final ok = await showConfirmDialog(
        context: context, content: tr(T.areyousuretologout));
    if (ok) HttpServiceImpl().logout();
  }
}
