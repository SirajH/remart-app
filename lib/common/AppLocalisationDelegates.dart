import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart' as intl;

class MaterialLocalizationAz extends DefaultMaterialLocalizations {
  static const List<String> _shortWeekdays = <String>[
    'B.E.',
    'Ç.A.',
    'Ç.',
    'C.A.',
    'C.',
    'Ş.',
    'B.',
  ];

  static const List<String> _shortMonths = <String>[
    'yan',
    'fev',
    'mar',
    'apr',
    'may',
    'i̇yn',
    'i̇yl',
    'avq',
    'sen',
    'okt',
    'noy',
    'dek'
  ];

  static const List<String> _narrowWeekdays = <String>[
    'Bz',
    'BE',
    'ÇA',
    'Çə',
    'CA',
    'Cü',
    'Şə',
  ];

  static const List<String> _months = <String>[
    'Yanvar',
    'Fevral',
    'Mart',
    'Aprel',
    'May',
    'İyun',
    'İyul',
    'Avqust',
    'Sentyabr',
    'Oktyabr',
    'Noyabr',
    'Dekabr'
  ];

  @override
  String get cancelButtonLabel => 'İMTİNA';

  @override
  String get okButtonLabel => 'OK';

  @override
  String formatMediumDate(DateTime date) {
    final String day = _shortWeekdays[date.weekday - DateTime.monday];
    final String month = _shortMonths[date.month - DateTime.january];
    return '${date.day} $month, $day';
  }

  @override
  List<String> get narrowWeekdays => _narrowWeekdays;

  @override
  String formatMonthYear(DateTime date) {
    final String year = formatYear(date);
    final String month = _months[date.month - DateTime.january];
    return '$month $year';
  }

  @override
  String pageRowsInfoTitle(int firstRow, int lastRow, int rowCount,
      bool rowCountIsApproximate) {
    return '$firstRow–$lastRow / $rowCount';
  }
}

class AppLocalizationsDelegate
    extends LocalizationsDelegate<MaterialLocalizations> {
  @override
  bool isSupported(Locale locale) {
    return true;
  }

  @override
  Future<MaterialLocalizations> load(Locale locale) {
    initializeDateFormatting('en', null);
    final String localeName = intl.Intl.canonicalizedLocale(locale.toString());

    if (locale.languageCode == 'az') {
      return SynchronousFuture(MaterialLocalizationAz());
    }
    intl.DateFormat fullYearFormat;
    intl.DateFormat mediumDateFormat;
    intl.DateFormat longDateFormat;
    intl.DateFormat yearMonthFormat;
    if (intl.DateFormat.localeExists(localeName)) {
      fullYearFormat = intl.DateFormat.y(localeName);
      mediumDateFormat = intl.DateFormat.MMMEd(localeName);
      longDateFormat = intl.DateFormat.yMMMMEEEEd(localeName);
      yearMonthFormat = intl.DateFormat.yMMMM(localeName);
    } else if (intl.DateFormat.localeExists(locale.languageCode)) {
      fullYearFormat = intl.DateFormat.y(locale.languageCode);
      mediumDateFormat = intl.DateFormat.MMMEd(locale.languageCode);
      longDateFormat = intl.DateFormat.yMMMMEEEEd(locale.languageCode);
      yearMonthFormat = intl.DateFormat.yMMMM(locale.languageCode);
    } else {
      fullYearFormat = intl.DateFormat.y();
      mediumDateFormat = intl.DateFormat.MMMEd();
      longDateFormat = intl.DateFormat.yMMMMEEEEd();
      yearMonthFormat = intl.DateFormat.yMMMM();
    }

    intl.NumberFormat decimalFormat;
    intl.NumberFormat twoDigitZeroPaddedFormat;
    if (intl.NumberFormat.localeExists(localeName)) {
      decimalFormat = intl.NumberFormat.decimalPattern(localeName);
      twoDigitZeroPaddedFormat = intl.NumberFormat('00', localeName);
    } else if (intl.NumberFormat.localeExists(locale.languageCode)) {
      decimalFormat = intl.NumberFormat.decimalPattern(locale.languageCode);
      twoDigitZeroPaddedFormat = intl.NumberFormat('00', locale.languageCode);
    } else {
      decimalFormat = intl.NumberFormat.decimalPattern();
      twoDigitZeroPaddedFormat = intl.NumberFormat('00');
    }

    if (locale.languageCode == 'en')
      return SynchronousFuture(MaterialLocalizationEn(
          decimalFormat: decimalFormat,
          fullYearFormat: fullYearFormat,
          longDateFormat: longDateFormat,
          mediumDateFormat: mediumDateFormat,
          twoDigitZeroPaddedFormat: twoDigitZeroPaddedFormat,
          yearMonthFormat: yearMonthFormat,
          localeName: localeName));
    else if (locale.languageCode == 'ru')
      return SynchronousFuture(MaterialLocalizationRu(
          decimalFormat: decimalFormat,
          fullYearFormat: fullYearFormat,
          longDateFormat: longDateFormat,
          mediumDateFormat: mediumDateFormat,
          twoDigitZeroPaddedFormat: twoDigitZeroPaddedFormat,
          yearMonthFormat: yearMonthFormat,
          localeName: localeName));

    return SynchronousFuture(DefaultMaterialLocalizations());
  }

  @override
  bool shouldReload(LocalizationsDelegate<MaterialLocalizations> old) {
    return true;
  }
}
