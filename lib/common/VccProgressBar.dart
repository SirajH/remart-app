import 'package:flutter/material.dart';

import '../theme.dart';

class VccProgressBar extends StatelessWidget {
  final Color color;
  final strokeWidth;
  const VccProgressBar({this.color = primaryColor, this.strokeWidth = 3.0});

  @override
  Widget build(BuildContext context) {
    return CircularProgressIndicator(
      valueColor: AlwaysStoppedAnimation<Color>(color),
      strokeWidth: strokeWidth,
    );
  }
}
