import 'package:flutter/material.dart';

import 'VccProgressBar.dart';

class VccProgressBarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: VccProgressBar()
      ),
    );
  }
}