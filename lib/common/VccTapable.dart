import 'package:flutter/material.dart';

class VccTapable extends StatelessWidget {
  final Widget child;
  final Function onTap;
  final double padding;
  static const double defaultPadding = 10;

  const VccTapable({Key key, @required this.child, this.onTap, this.padding = defaultPadding}) : super(key: key);

  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        child: Container(
          padding: EdgeInsets.all(padding),
          child: child
        ),
        onTap: onTap,
      ),
    );
  }
}
