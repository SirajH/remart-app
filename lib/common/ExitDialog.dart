import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/service/TranslationService.dart';

import 'T.dart';

class ExitDialog extends StatelessWidget {
  final child;
  final NavBloc bloc;

  ExitDialog({
    @required this.child,
    this.bloc,
  }) : assert(child != null);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (bloc == null || bloc.back()) {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  title: Text(tr(T.prompt)),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(tr(T.popscopequestion)),
                    ],
                  ),
                  actions: [
                    FlatButton(
                        child: Text(tr(T.imtina)),
                        onPressed: () {
                          Navigator.pop(context, false);
                        }),
                    FlatButton(
                        child: Text('OK'),
                        onPressed: () {
                          SystemChannels.platform
                              .invokeMethod('SystemNavigator.pop');
                        }),
                  ],
                ),
          );
        }
      },
      child: child,
    );
  }
}
