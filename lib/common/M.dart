class M {
  static const en = 'EN';
  static const ru = 'RU';
  static const az = 'AZ';

  // SUCCESS
  static final ok = 'ok';
  static final success = 'success';

  // ERRORS
  static final unexpected = 'unexpected';
  static final http400 = 'err.bad_request';
  static final http401 = 'err.logged_out';
  static final http403 = 'err.access_denied';
  static final http404 = 'err.not_found';
  static final http405 = 'err.not_allowed';
  static final http409 = 'err.conflict';
  static final http504 = 'err.gateway_timeout';
  static final http0 = 'err.check_internet';

  static const noInternet = 'no_internet';
  static const connectionTimeout = 'connection_timeout';
  static const somethingWrong = 'something_went_wrong';
  static const errorOccurred = 'error_occurred';

  // Validator errors
  static final emptyInput = 'empty_input';
  static final regexNotMatch = 'regex_not_match';
  static final repeatingChars = 'repeating_chars';
  static final invalidLength = 'invalid_length';
  static final valuesNotMatch = 'values_not_match';
  static final creditLimitErr1 = 'credit_limit_err_1';
  static final creditLimitErr2 = 'credit_limit_err_2';
  static final conditionsNotMatch = 'conditions_not_match';

}