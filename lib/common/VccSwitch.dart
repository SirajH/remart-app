import 'package:flutter/material.dart';
import 'package:remart/service/TranslationService.dart';

import '../theme.dart';
import 'T.dart';

class VccSwitch extends StatelessWidget {
  final SwitchItem data;
  final Function onChange;

  VccSwitch({this.data, this.onChange});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
          data == null || data.showProgress
            ? Text(
                tr(T.inprogress),
                style: cardSubheader,
              )
            : SizedBox(),
        Switch(
          value: data != null && data.state,
          activeColor: Colors.green,
          onChanged:
              data != null && !data.disabled ? (value) => onChange(value) : null,
        ),
      ],
    );
  }
}

class SwitchItem {
  bool state;
  bool disabled;
  bool showProgress;
  String value;

  SwitchItem(this.state, this.disabled, {this.value, this.showProgress}){
    showProgress = showProgress == null ? disabled : showProgress;
  }

  @override
  String toString() {
    return 'SwitchItem{state: $state, disabled: $disabled, showProgress: $showProgress, value: $value}';
  }



}
