import 'dart:async';

import 'package:flutter/material.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/bloc/MessageBloc.dart';
import 'package:remart/model/Msg.dart';

import 'VccProgressBar.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  LogoutBloc _logoutBloc = LogoutBloc();
  MessageBloc _msgBloc = MessageBloc();

   StreamSubscription subs;

  @override
  void initState() {
    subs = _logoutBloc.errorStream.listen((String msg) {
      _msgBloc.showMsg(Msg(msg));
     
    });
    super.initState();
  }

  @override
  void dispose() {
    subs.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
//            Image.asset('assets/img/favicon.png', width: 60, height: 60),
            SizedBox(height: 12),
            Text(
              'Remart',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: 18,
            ),
            SizedBox(height: 20, width: 20, child: VccProgressBar()),
          ],
        ),
      ),
    );
  }
}
