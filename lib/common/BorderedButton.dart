import 'package:flutter/material.dart';
import 'package:remart/common/VccProgressBar.dart';
import 'package:remart/model/Status.dart';

import '../theme.dart';

class BorderedButton extends StatelessWidget {
  final text;
  final icon;
  final onTap;
  final Status status;
  BorderedButton({this.text, this.icon, this.onTap, this.status});

  @override
  Widget build(BuildContext context) {
    return OutlineButton(
      padding: EdgeInsets.symmetric(vertical: pad6, horizontal: pad10),
      child: status == Status.LOADING
          ? Container(
              width: 20,
              height: 20,
              child: VccProgressBar())
          : Row(
              children: [
                Icon(icon, size: 18, color: primaryColor),
                SizedBox(width: pad4),
                Text(text, style: TextStyle(fontSize: 11, color: primaryColor))
              ],
            ),
      highlightedBorderColor: primaryColor,
      borderSide: BorderSide(color: primaryColor),
      shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(20)),
      onPressed: status == Status.LOADING ? null : onTap,
    );
  }
}
