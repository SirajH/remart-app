import 'package:flutter/src/widgets/framework.dart';
import 'package:remart/common/T.dart';
import 'package:remart/service/TranslationService.dart';

import 'M.dart';

class InputChecker {
  final input;
  final List<String> rules;

  InputChecker(this.input, this.rules);

  validate() {
    for (var rule in rules) {
      if (rule != null) return rule;
    }
    return null;
  }
}

class Validator {
  static String input;

  static value(input) => {Validator.input = input};

  static get required => input.trim().isEmpty ? M.emptyInput : null;

  static min(min) => input.length < min ? M.invalidLength : null;

  static max(max) => input.length > max ? M.invalidLength : null;

  static maxInt(int max) {
    if (int.tryParse(input) == null) return M.regexNotMatch;
    return int.parse(input) > max ? M.invalidLength : null;
  }

  static length(length) => input.length != length ? M.invalidLength : null;

  static match(value) => input != value ? M.valuesNotMatch : null;

  static regex(regex, {msg, match = false}) =>
      (match ? RegExp(regex).hasMatch(input) : !RegExp(regex).hasMatch(input))
          ? (msg != null ? msg : M.regexNotMatch)
          : null;

  static and(List<bool> conditions, {msg}) {
    bool res = true;
    for (bool c in conditions) res = res && c;
    if (res) return msg != null ? msg : M.conditionsNotMatch;
    return null;
  }

  static msisdn(msisdn) {
    String m = InputChecker(Validator.value(msisdn),
        [Validator.required, Validator.regex(r'^\+[1-9]{1}[0-9]{3,14}$')]).validate();

    final errors = {
      M.emptyInput: tr(T.inputmsisdn),
      M.regexNotMatch: tr(T.wrongmsisdn)
    };
    return errors[m];
  }

  static String validateBudget(String val, bool required) {
    Validator.value(val);

    List<String> roles = [];

    if (required) {
      roles
        ..add(Validator.required)
        ..add(Validator.regex('^[0-9]+\$'))
        ..add(Validator.maxInt(1000000000));
    }

    if (!required && val.isNotEmpty) {
      roles
        ..add(Validator.regex('^[0-9]+\$'))
        ..add(Validator.maxInt(1000000000));
    }
    String m = InputChecker(val, roles).validate();
    final errors = {
      M.emptyInput: tr(T.inputbudget),
      M.regexNotMatch: tr(T.wrongbudget),
      M.invalidLength: tr(T.invalidLength)  // TODO: wrong error message, should be specific
    };
    return errors[m];
  }

  static String nonEmpty(String val) {
    Validator.value(val);
    String m = InputChecker(val, [Validator.required]).validate();
    final errors = {
      M.emptyInput: tr(T.nonempty),
    };
    return errors[m];
  }

  static String email(String val) {
    Validator.value(val);
    String m = InputChecker(val, [
      Validator.required,
      Validator.regex(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$'),
    ]).validate();
    final errors = {
      M.emptyInput: tr(T.nonempty),
      M.regexNotMatch: tr(T.wrongemail),
    };
    return errors[m];
  }

  static String password(val) {
    Validator.value(val);
    String m =
        InputChecker(val, [Validator.required, Validator.min(6)]).validate();
    final errors = {
      M.emptyInput: tr(T.nonempty),
      M.invalidLength: tr(T.invalidLength, items: ['6']), // TODO: this should be returned from validator
    };
    return errors[m];
  }

  static String name(String val) {
    Validator.value(val);
    String m =
        InputChecker(val, [Validator.required, Validator.min(2)]).validate();
    final errors = {
      M.emptyInput: tr(T.nonempty),
      M.invalidLength: tr(T.invalidLength, items: ['2']), // TODO: this should be returned from validator
    };
    return errors[m];
  }
}

class Validate {
  List<Function> funcs;
  dynamic value;

  List<String> errors;

  Validate(this.value, this.funcs);

  String validate() {
    for (var i = 0; i < funcs.length; i++) {
      String res = Function.apply(funcs[i], [value]);
      if (res != null) return res;
    }
    return null;
  }

  static String msisdn(value) {
    String r = regex(value, r'^$|\+[0-9]{12}$');
    return r != null ? tr(T.wrongmsisdn) : null;
  }

  static String email(value) {
    String r = regex(value, r'^$|(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
    return r != null ? tr(T.wrongemail) : null;
  }


  static String required(value) =>
      (value == null || value == '') ? T.nonempty : null;

  static regex(value, regex, {match = false}) =>
      (match ? RegExp(regex).hasMatch(value) : !RegExp(regex).hasMatch(value))
          ? M.regexNotMatch
          : null;
}
