import 'package:flutter/material.dart';
import 'package:remart/common/T.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/service/Util.dart';

class SomethingWrong extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    showError(tr(T.unexpectederror));
    return SizedBox();
  }
}