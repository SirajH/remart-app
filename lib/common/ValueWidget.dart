import 'package:flutter/material.dart';
import 'package:remart/service/Util.dart';

import '../theme.dart';

class ValueWidget extends StatelessWidget {
  final String label;
  final String value;
  final double paddingBottom;
  final TextStyle style;

  const ValueWidget(this.label, this.value,
      {Key key, this.paddingBottom = 12, this.style = balanceStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(safeToUpperCase(label),
            style: TextStyle(fontSize: 11, color: Colors.grey[600])),
        SizedBox(height: 3),
        Text(value, style: style),
        SizedBox(height: paddingBottom),
      ],
    );
  }
}
