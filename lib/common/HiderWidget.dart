import 'package:flutter/material.dart';

class HiderWidget extends StatelessWidget {
  final bool visible;
  final Function show;

  HiderWidget({
    @required this.visible,
    @required this.show,
  })  : assert(visible != null),
        assert(show != null);

  @override
  Widget build(BuildContext context) => visible ? show() : SizedBox();
}