import 'package:flutter/material.dart';
import 'package:remart/bloc/NavBloc.dart';
import 'package:remart/model/NavData.dart';
import 'package:remart/screen/CoreView.dart';
import 'package:remart/service/TranslationService.dart';

import 'T.dart';

class MyBottomNavBar extends StatelessWidget {
  final index;
  final navBloc = NavBloc();

  MyBottomNavBar({Key key, this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      currentIndex: index,
      onTap: (index) {
        navBloc.changeRoute(NavData(index));
      },
      items: [
        BottomNavigationBarItem(
            title: Text(tr(T.requests)), icon: Icon(Icons.list)),
        BottomNavigationBarItem(
            title: Text(tr(T.myrequests)), icon: Icon(Icons.assignment)),
        BottomNavigationBarItem(
            title: Text(tr(T.myprofile)), icon: Icon(Icons.account_circle)),
        BottomNavigationBarItem(
            title: Text(tr(T.realtors)), icon: Icon(Icons.people)),
      ],
    );
  }
}