import 'package:flutter/material.dart';
import 'package:remart/model/Status.dart';
import 'package:remart/service/TranslationService.dart';

import '../theme.dart';
import 'M.dart';
import 'T.dart';

class RemartButton extends StatelessWidget {
  final text;
  final onTap;
  final Status status;
  final bool primary;
  final bool border;
  final double horizontalPad;
  final double verticalPad;
  final MainAxisAlignment alignment;

  const RemartButton(
      {this.text,
        this.onTap,
        Key key,
        this.status,
        this.primary = true,
        this.border = true,
        this.horizontalPad = 20,
        this.verticalPad = 12,
        this.alignment = MainAxisAlignment.center})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisAlignment: alignment,
        children: <Widget>[
          Material(
            color: primary ? primaryColor : Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(4)),
            child: InkWell(
              //onTap: status == Status.OK ? onTap : null,
              onTap: status == Status.LOADING ? null : onTap,
              child: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: horizontalPad, vertical: verticalPad),
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  border:
                  Border.all(color: border ? primaryColor : Colors.transparent),
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                ),
                child: Center(
                  child: status == Status.LOADING
                      ? Container(
                      width: 20,
                      height: 20,
                      child: CircularProgressIndicator(
                        strokeWidth: 3,
                        valueColor: AlwaysStoppedAnimation<Color>(
                            primary ? Colors.white : primaryColor),
                      ))
                      : Text(text,
                      style: TextStyle(
                          color: primary ? Colors.white : primaryColor,
                          fontWeight: FontWeight.w600)),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class RemartCard extends StatelessWidget {
  final Widget child;

  RemartCard({this.child});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [BoxShadow(color: Color(0x22000000), blurRadius: 8.0)],
            borderRadius: BorderRadius.all(Radius.circular(4))),
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: child,
        ),
      ),
    );
  }
}

class VccCardWithHeader extends StatelessWidget {
  final Widget child;
  final Widget headerChild;
  final double padding;

  VccCardWithHeader({this.child, this.headerChild, this.padding = 20.0});

  @override
  Widget build(BuildContext context) {
    final bool hasChild = child != null;

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Container(
        decoration: BoxDecoration(
            boxShadow: [BoxShadow(color: Color(0x22000000), blurRadius: 8.0)]),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: Container(
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: hasChild ? Color(0xFFF0F0F0) : Colors.white),
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: headerChild,
                  ),
                ),
                hasChild
                    ? Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: padding, vertical: padding),
                        child: child,
                      )
                    : SizedBox(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class HeaderContent extends StatelessWidget {
  final header;
  final subHeader;

  HeaderContent({this.header, this.subHeader});

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      Text(header, style: cardHeaderStyle),
      SizedBox(height: 4),
      Text(subHeader, style: cardSubheader)
    ]);
  }
}

class VccScaffold extends StatelessWidget {
  final child;
  final Function localeSelected;
  final Widget leading;

  VccScaffold({this.child, this.localeSelected, this.leading});

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      appBar: AppBar(
          leading: leading,
          title: Text('Remart', style: titleStyle),
          actions: [LanguagePopup(onSelected: localeSelected)]),
      body: SingleChildScrollView(
        child: Container(width: double.infinity, child: child),
      ),
    );
  }
}

class LanguagePopup extends StatelessWidget {
  final Function onSelected;

  LanguagePopup({this.onSelected});

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      itemBuilder: (context) {
        return [
          PopupMenuItem(value: TranslationService.az, child: Text(M.az)),
          PopupMenuItem(value: TranslationService.ru, child: Text(M.ru)),
          PopupMenuItem(value: TranslationService.en, child: Text(M.en)),
        ];
      },
      onSelected: (val) => onSelected(val),
      icon: Icon(Icons.more_vert),
    );
  }
}

Widget horizontalLine() {
  return Container(height: 1, color: hrColor);
}

showConfirmDialog({context, content, title}) async {
  final ok = await showDialog(
    context: context,
    barrierDismissible: false,
    builder: (context) => AlertDialog(
          title: Text(title != null ? title : tr(T.prompt)),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(content),
            ],
          ),
          actions: [
            FlatButton(
                child: Text(tr(T.imtina)),
                onPressed: () => Navigator.pop(context, false)),
            FlatButton(
                child: Text('OK'),
                onPressed: () => Navigator.pop(context, true)),
          ],
        ),
  );
  return ok;
}
