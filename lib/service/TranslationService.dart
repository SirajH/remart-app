import 'dart:convert';

import 'package:remart/common/M.dart';
import 'package:remart/common/T.dart';
import 'package:remart/common/TrMap.dart';
import 'package:remart/model/DealType.dart';
import 'package:remart/model/RequestsType.dart';

import 'HttpService.dart';
import 'SharedPrefsService.dart';

class TranslationService {
  static final TranslationService _singletone = TranslationService._();

  TranslationService._();

  factory TranslationService() => _singletone;

  static final az = 'az';
  static final ru = 'ru';
  static final en = 'en';

  static final langs = [
    {'lang': 'az', 'value': 'Azərbaycan dili'},
    {'lang': 'ru', 'value': 'Русский'},
    {'lang': 'en', 'value': 'English'},
  ];


  final sps = SharedPrefsService();

  static Map<String, dynamic> map = TrMap.map;
  String locale;

  String tr(String key, List<String> items) {
    if (!map.containsKey(key)) return '@$key';
    String text = '${map[key][locale]}';
    if (items != null && items.length > 0) {
      items.forEach((s) => text = text.replaceFirst('#replace', s));
    }
    return text;
  }

  Future<bool> handleTranslations() async {
    final _http = HttpServiceImpl();
    String currVersion = await sps.getString(SharedPrefsService.trVersion);
    String version = '${await _http.getTranslationsVersion()}' ?? '1';
//    print('>>>> currVer: $currVersion, version: $version');

    if (currVersion == null || currVersion != version) {
      sps.setString(SharedPrefsService.trVersion, version);
    }

    if (currVersion == version) {
      getTrMapFromSp();
      return true;
    }

    Map<String, dynamic> transMap = await _http.getTranslations();

    if (transMap != null) {
      String trans = json.encode(transMap);
      await sps.setString(SharedPrefsService.transKey, trans);
      map = transMap['Map'];
    }
    return true;
  }

  handleAppLang() async {
    // get current language
    String currLang = await sps.getString(SharedPrefsService.lang);
    currLang = currLang ?? az;
    locale = currLang;

    // get Translations
    getTrMapFromSp();
  }

  getTrMapFromSp() async {
    String trans = await sps.getString(SharedPrefsService.transKey);
    if (trans == null) return;
    final m = json.decode(trans)['Map'];
    if (m is List && m.length == 0) return;
    map = m;
  }

  changeLocale(locale) {
    this.locale = locale;
    sps.setString(SharedPrefsService.lang, locale);
  }
}

String tr(String key, {items}) => TranslationService().tr(key, items).trim();

String trs(String key, {items, suffix: ': '}) =>
    TranslationService().tr(key, items) + suffix;

class C {
  static final min = 'min';
  static final max = 'max';
  static final exists = 'exists';
  static final nonexists = 'nonexists';
  static final m2 = 'm²';
  static final kupchie = 'kupchie';
  static final repair = 'repair';
  static final room = 'room';
  static final area = 'area';
  static final gas = 'gas';
  static final price = 'price';
  static final city = 'city';
  static final district = 'district';
  static final village = 'village';
  static final metro = 'metro';
  static final orient = 'orient';
  static final m = '₼';
  static final rm = 'rm';
  static final withrepair = 'withrepair';
  static final withoutrepair = 'withoutrepair';
  static const share = 'share';
  static const taketowork = 'taketowork';
  static const edit = 'edit';
  static const deactivate = 'deactivate';
  static const activate = 'activate';
  static const delete = 'delete';
  static const today = 'today';
}
