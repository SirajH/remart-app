import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/common/T.dart';
import 'package:remart/model/DealType.dart';
import 'package:remart/model/RequestsType.dart';
import 'package:remart/service/TranslationService.dart';
import 'package:remart/theme/remart_theme.dart';

import 'SessionService.dart';

class Util {
  static normalize(int val1, int val2, {suffix = ''}) {
    final ts = TranslationService();
    if (val1 == null && val2 == null) return '';
    if (val1 == 0 && val2 == 0) return '';
    if (val1 == val2) {
      return '$val1 $suffix';
    } else if (val2 == null || val2 == 0) {
      return '${tr(C.min)} $val1 $suffix';
    } else if (val1 == null || val1 == 0) {
      return '${tr(C.max)} $val2 $suffix';
    } else {
      return '$val1-$val2 $suffix';
    }
  }

  static TextSpan normalizewithTS(int val1, int val2, {String suffix = ''}) {
    final text = Util.normalize(val1, val2);
    return text == ''
        ? null
        : TextSpan(children: [
            TextSpan(text: text, style: bold),
            TextSpan(text: '$suffix'),
          ]);
  }

  static TextSpan addTSStyle(val, {String suffix = ''}) {
    return TextSpan(children: [
      TextSpan(text: val.toString(), style: bold),
      TextSpan(text: '$suffix'),
    ]);
  }

  static String getRichTextValue(RichText richText) {
    final rts = richText.toString();
    return rts.substring(rts.indexOf('text: "') + 7, rts.length - 2);
  }
}

final currencies = {1: '₼', 2: '\$', 3: '€', 4: 'ლ', 5: '₸'};

String getRelativeDate(DateTime date) {
  date = date.toLocal();
  final res = isToday(date)
      ? tr(T.today) + ' ' + DateFormat('kk:mm').format(date)
      : DateFormat('dd.MM.yyyy kk:mm').format(date);
  return res;
}

bool isToday(DateTime date) =>
    DateTime.now().day == date.day &&
    DateTime.now().month == date.month &&
    DateTime.now().year == date.year;

showPrompt(context, title, content, {barrierDismissible = true}) async {
  return await showDialog(
    context: context,
    barrierDismissible: barrierDismissible,
    builder: (BuildContext context) {
      return AlertDialog(
        title: Text(title),
        content: Text(content),
        actions: [
          FlatButton(
            child: Text(tr(T.ok)),
            onPressed: () async {
              Navigator.pop(context, true);
            },
          ),
          FlatButton(
            child: Text(tr(T.imtina)),
            onPressed: () => Navigator.pop(context, false),
          ),
        ],
      );
    },
  );
}

String getDealType(id) {
  final val = DealType.values[id - 1];
  return safeToUpperCase(tr('$val'));
}

String getSorguType(id) {
  final val = SorguType.values[id - 1];
  return safeToUpperCase(tr('$val'));
}

get divider => Divider(color: Colors.grey);

void pd(String text) => print(text);

showError(error) async {
  await Future.delayed(Duration(seconds: 1), () {});
  LogoutBloc().showErrorPage(error);
}

safeToUpperCase(String text) {
  final locale = TranslationService().locale;
  if (locale == 'az')
    return text.replaceAll('i', 'İ').toUpperCase();
  else
    return text.toUpperCase();
}

hideKeyboard(context) async {
  FocusScope.of(context).requestFocus(new FocusNode());
  await Future.delayed(Duration(milliseconds: 500), () {});
}

isMe(int id) => SessionService.id != null && id == SessionService.id;

DropdownMenuItem<int> get egal => DropdownMenuItem(
    child: Text(tr(T.ferqetmez), style: TextStyle(color: Colors.grey)), value: 0);

SliverToBoxAdapter get sliverEmpty => SliverToBoxAdapter(child: SizedBox());


isSorgu(int dealTypeId) => dealTypeId > 0 && dealTypeId < 4;