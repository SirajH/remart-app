import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:remart/bloc/LogoutBloc.dart';
import 'package:remart/bloc/MessageBloc.dart';
import 'package:remart/common/M.dart';
import 'package:remart/model/AppVersion.dart';
import 'package:remart/model/DetailedLoginResponse.dart';
import 'package:remart/model/Msg.dart';
import 'package:remart/model/Sorgu.dart';
import 'package:remart/model/SorguFilter.dart';

import 'SessionService.dart';

class HttpServiceImpl {
//  final String base = 'http://localhost:8888/remart-app-backend/public/api';
  final String base = 'https://app.remart.az/api';

  final appVersion = '1.0.9+10';

  final Dio http = Dio();
  final logoutBloc = LogoutBloc();
  final messageBloc = MessageBloc();

  static final HttpServiceImpl _singletone = HttpServiceImpl._();

  factory HttpServiceImpl() => _singletone;

  HttpServiceImpl._() {
    createInterceptor();
  }

  createInterceptor() {
    http.options.connectTimeout = 30000;
    http.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      print('<${options.method}> ${options.uri}');
      options.headers['Accept'] = 'application/json';
      options.headers['Authorization'] = 'Bearer ${SessionService.token}';
      options.headers['V-Source'] = Platform.isIOS ? 'ios' : 'android';
      options.headers['V-Version'] = appVersion;
      return options;
    }, onResponse: (Response response) {
//      print('intercept onResponse : $response');

      return response;
    }, onError: (DioError error) {
      print('intercept onError : $error  ${error.response?.data}');

      if (error is DioError) {
        String msg;
        switch (error.type) {
          case DioErrorType.DEFAULT:
            msg = M.noInternet;
            break;
          case DioErrorType.CONNECT_TIMEOUT:
            msg = M.connectionTimeout;
            break;
          case DioErrorType.CANCEL:
            msg = M.unexpected;
            break;
          case DioErrorType.SEND_TIMEOUT:
            msg = M.connectionTimeout;
            break;
          case DioErrorType.RECEIVE_TIMEOUT:
            msg = M.connectionTimeout;
            break;
          case DioErrorType.RESPONSE:
            Msg data = _handleHttpStatusCode(error.response?.statusCode);
            // TODO: Remove if and find better solution
            if (data.msg == M.http409 ||
                data.msg == M.http401 ||
                data.msg == M.http400) {
              return http.resolve(Response(
                  data: error.response.data,
                  statusCode: error.response?.statusCode));
            } else {
              messageBloc.showMsg(data);
              return http.resolve(
                  Response(data: data, statusCode: error.response?.statusCode));
            }
            break;
          default:
            msg = M.unexpected;
            break;
        }

        showError(msg);
        return http.resolve(Response(data: msg, statusCode: 600));
      } else {
        logout();
        return null;
      }
    }));
  }

  showError(error) async {
    await Future.delayed(Duration(seconds: 1), () {});
    LogoutBloc().showErrorPage(error);
  }

  Msg _handleHttpStatusCode(statusCode) {
    String msg = '';
    switch (statusCode) {
      case 401:
        msg = M.http401;
        logout();
        break;
      case 0:
        msg = M.http0;
        break;
      case 400:
        msg = M.http400;
        break;
      case 403:
        msg = M.http403;
        break;
      case 404:
        msg = M.http404;
        break;
      case 405:
        msg = M.http405;
        logout();
        break;
      case 409:
        msg = M.http409;
        break;
      case 504:
        msg = M.http504;
        break;
      default:
        msg = M.unexpected;
    }
    return Msg(msg);
  }

  Future<dynamic> login(String username, String password) async {
    final url = '$base/login';
    final response = await http.post(url,
        data: FormData.from({"username": username, "password": password}));
    if (response.statusCode == 200) {
      return DetailedLoginResponse.fromJson(response.data);
    } else if (response.statusCode == 401) {
      return response.data['error'];
//      LogoutBloc().showErrorPage(response.data['error']);
    }
    return null;
  }

  logout() {
    logoutBloc.logout();
//    final url = '$base/logout';
//    http.post(url, data: FormData.from({}));
  }

  Future<Sorgu> getRequest(int id) async {
    final url = '$base/request/$id';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return Sorgu.fromJson(response.data);
    }
    return null;
  }


  Future<List<Sorgu>> filterRequests({SorguFilter filter, skip}) async {
    print('>>> filterRequests: $filter');
    final url = '$base/requests/filter/$skip';
    final response = await http.post(url, data: filter.toJson());
    if (response.statusCode == 200) {
      print('filterRequests request length> ${(response.data as List).length}');
      return List<Sorgu>.from(response.data.map((x) => Sorgu.fromJson(x)));
    }
    return [];
  }

  Future<List<Sorgu>> filterMyRequests({SorguFilter filter, skip}) async {
    final url = '$base/myrequests/$skip';
    final response = await http.post(url, data: filter.toJson());
    if (response.statusCode == 200) {
      return List<Sorgu>.from(response.data.map((x) => Sorgu.fromJson(x)));
    }
    return null;
  }

  Future<List<Sorgu>> getInProgressRequests({int requestId = -1}) {
    return Future.delayed(Duration(milliseconds: 500), () => []);
  }

  Future<bool> addInProgressRequest(Sorgu r, String note) {
    return Future.delayed(Duration(milliseconds: 1), () => true);
  }

  Future<bool> addRequest(Sorgu request) {
    return Future.delayed(Duration(seconds: 1), () => true);
  }

  Future<Realtor> getMyProfile() async {
    final url = '$base/myprofile';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return Realtor.fromJson(response.data);
    }
    return null;
  }

  updatePersonalNote(int id, String note) {}

  Future<bool> removeFromInprogress(int id) {}

  Future<bool> changeStatus(int id, int status) {}

  Future<List<Country>> getCountries() async {
    final url = '$base/countries';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return List<Country>.from(response.data.map((x) => Country.fromJson(x)));
    }
    return [];
  }

  Future<List<City>> getCities(int countryId) async {
    print('MC: Http getCities');
    final url = '$base/cities/$countryId';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return List<City>.from(response.data.map((x) => City.fromJson(x)));
    }
    return [];
  }

  Future<List<District>> getDistricts(int cityId) async {
    print('MC: Http getDistricts');
    final url = '$base/districts/$cityId';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return List<District>.from(
          response.data.map((x) => District.fromJson(x)));
    }
    return [];
  }

  Future<List<District>> getVillages(int cityId) async {
    print('MC: Http getVillages');
    final url = '$base/villages/$cityId';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return List<District>.from(
          response.data.map((x) => District.fromJson(x)));
    }
    return [];
  }

  Future<List<District>> getMetros(int cityId) async {
    print('MC: Http getMetros');
    final url = '$base/metros/$cityId';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return List<District>.from(
          response.data.map((x) => District.fromJson(x)));
    }
    return [];
  }

  Future<bool> createRequest(Sorgu sorgu) async {
    final url = '$base/requests';
    final response = await http.post(url, data: sorgu.toJson());
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('result> ${response.data}');
      return true;
    }
    return false;
  }

  Future<bool> update(Map<String, dynamic> data) async {
    final url = '$base/requests/${data['id']}';
    final response = await http.put(url, data: data);
    if (response.statusCode == 200 || response.statusCode == 201) {
//      print('result> ${response.data}');
      return true;
    }
    return false;
  }

  Future<bool> updateRequest(Sorgu sorgu) async {
    return update(sorgu.toJson());
  }

  Future<bool> changeRequestStatus(int id, int status) {
    return update({'id': id, 'status': status});
  }

  Future<bool> updateProfile(Realtor realtor) async {
    final url = '$base/realtors';
    final response = await http.put(url, data: realtor.toJson());
    if (response.statusCode == 200 || response.statusCode == 201) {
      print('result> ${response.data}');
      return true;
    }
    return false;
  }

  Future<List<Realtor>> getRealtors({skip: -1, String word}) async {
    final url =
        '$base/realtors/$skip/$word?country_id=${SessionService.countryId}';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return List<Realtor>.from(response.data.map((x) => Realtor.fromJson(x)));
    }
    return [];
  }

  Future<Map<String, dynamic>> getTranslations() async {
    final url = '$base/get_tr';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return response.data;
    }
    return null;
  }

  Future<int> getTranslationsVersion() async {
    final url = '$base/get_tr_version';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return response.data[0]['version'];
    }
    return null;
  }

  Future<String> register({String msisdn, String password, String name}) async {
    final url = '$base/register';
    final response = await http.post(url,
        data: FormData.from(
            {"username": msisdn, "name": name, "password": password}));

    if (response.statusCode == 200) {
      return 'ok';
    } else if (response.statusCode == 400) {
      return response.data[0];
    }
    return null;
  }

  Future<AppVersion> getAppVersion() async {
    final url = '$base/get_app_version';
    final response = await http.get(url);
    if (response.statusCode == 200) {
      return AppVersion.fromJson(response.data);
    }
    return null;
  }
}
