import 'package:remart/model/Sorgu.dart';

class SessionService {

  static int countryId;

  static bool get isLogged => login != null;
  static int get id => SessionService.user?.id;

  static String login;
  static String token;
  static Realtor user;

  @override
  String toString() {
    return 'SessionService{login: $login, token: $token, user: $user}';
  }

  static void logout() {
    login = null;
    token = null;
    user = null;
  }

}