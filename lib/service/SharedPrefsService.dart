import 'dart:convert';

import 'package:remart/model/DetailedLoginResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SessionService.dart';

class SharedPrefsService {
  static final String loginResponse = "login_reponse";
  static final String loginKey = "login";
  static final String transKey = "trans";
  static final String trVersion = "trVersion";
  static final String lang = "lang";
  static final String country = "country";

  static final _singletone = SharedPrefsService._internal();

  factory SharedPrefsService() {
    return _singletone;
  }

  SharedPrefsService._internal();

  addLoginResponse(String login, DetailedLoginResponse dlr) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    sp.setString(loginResponse, json.encode(dlr));
    sp.setString(loginKey, login);
    SessionService.login = login;
    SessionService.token = dlr.token;
    SessionService.user = dlr.user;
  }

  Future<DetailedLoginResponse> getLoginResponse() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    String jsonDlr = sp.getString(loginResponse);
    String login = sp.getString(loginKey);
//    print('getLoginResponse $jsonDlr');

    if (jsonDlr != null && login != null) {
      final dlr = DetailedLoginResponse.fromJson(json.decode(jsonDlr));
      SessionService.login = login;
      SessionService.token = dlr.token;
      SessionService.user = dlr.user;
      print('getLoginResponse $dlr');
      return dlr;
    }
    return null;
  }

  getCountry() async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    final countryId = sp.getInt(SharedPrefsService.country);
    if (countryId != null) {
      SessionService.countryId = countryId;
    }
  }

  removeLoginResponse() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(loginResponse);
  }

  remove(String field) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(field);
  }

  Future setField(String field, int mnl) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(field, mnl);
  }

  Future<int> getField(String field) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(field);
  }

  Future<String> getString(String field) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(field);
  }

  Future<bool> setString(String field, String value) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return await sharedPreferences.setString(field, value);
  }
}
