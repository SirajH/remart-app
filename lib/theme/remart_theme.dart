import 'package:flutter/material.dart';

final linkColor = Color(0xff5a93f7);
final linkStyle = TextStyle(color: linkColor);
final mPadding = 20.0;
final iconColor = Color(0xff5b6173);
final textColor = Color(0xff555555);
final textStyle = TextStyle(color: textColor);
final lightGrayIconColor = Color(0xffc5c7cd);
final appbarElev = 0.5;
final primaryBlue = Color(0xff374260);
final bgColor = Color(0x103F51B5);
final bold = const TextStyle(fontWeight: FontWeight.bold);
final typeColors = const {
  0: Color(0xFFB71C1C),
  1: Color(0xFF5C6BC0),
  2: Color(0XFF884B68),
  3: Color(0xFF7E57C2),
  4: Color(0xFF42A5F5),
  5: Color(0xFF1B5E20),
  6: Color(0xFF1A237E),
  7: Color(0xFF880E4F),
  8: Color(0xFF880E4F),
  9: Color(0xFF880E4F),
  10: Color(0xFF880E4F),
  11: Color(0xFF880E4F),
  12: Color(0xFF880E4F),
};
