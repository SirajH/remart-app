import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:remart/screen/CoreView.dart';
import 'package:remart/screen/LoginPage.dart';
import 'package:remart/theme.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'common/AppLocalisationDelegates.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey =
      new GlobalKey<NavigatorState>();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

  MyApp() {
    _firebaseMessaging.requestNotificationPermissions(
        IosNotificationSettings(sound: true, badge: true, alert: true));
    _firebaseMessaging.onIosSettingsRegistered
        .listen((IosNotificationSettings settings) {
//      print("Settings registered: $settings");
    });

    _firebaseMessaging.getToken().then((token) {
      print('token>' + token);
    });

    _firebaseMessaging
        .subscribeToTopic('remart_new_requests')
        .then((_) => print('>>> subscribed to topic'));

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        var _messageText = "Push Messaging message: $message";
        print("onMessage: $_messageText");
      },
      onLaunch: (Map<String, dynamic> message) async {
        var _messageText = "Push Messaging message: $message";
        print("onLaunch: $_messageText");
      },
      onResume: (Map<String, dynamic> message) async {
        var _messageText = "Push Messaging message: $message";
        print("onResume: $_messageText");
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      locale: Locale('az'),
      title: 'Remart',
      debugShowCheckedModeBanner: false,
      navigatorKey: navigatorKey,
      initialRoute: '/',
//        navigatorObservers: <NavigatorObserver>[observer],
      theme: ThemeData(primaryColor: primaryColor, accentColor: accentColor),
      routes: {
//        '/': (context) => Test(),
        '/': (context) => CoreView(navigatorKey),
        '/login': (context) => LoginPage(),
      },
      localizationsDelegates: [
        AppLocalizationsDelegate(),
        // Workaround to fix "The getter 'pasteButtonLabel' was called on null."
        FallbackCupertinoLocalisationsDelegate()
      ],
      supportedLocales: [
        const Locale('az'),
        const Locale('en', 'US'), // English
        const Locale('ru'), // Arabic
      ],
    );
  }
}

class FallbackCupertinoLocalisationsDelegate
    extends LocalizationsDelegate<CupertinoLocalizations> {
  const FallbackCupertinoLocalisationsDelegate();

  @override
  bool isSupported(Locale locale) => true;

  @override
  Future<CupertinoLocalizations> load(Locale locale) =>
      DefaultCupertinoLocalizations.load(locale);

  @override
  bool shouldReload(FallbackCupertinoLocalisationsDelegate old) => false;
}

class Test extends StatefulWidget {
  @override
  _TestState createState() => _TestState();
}

class _TestState extends State<Test> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
//          IntrinsicHeight(
//            child: Row(
//              children: <Widget>[
//                ListView(
//              shrinkWrap: true,
//                  scrollDirection: Axis.horizontal,
//                  children: [
//                    Container(
////                  height: double.infinity,
//                      decoration: BoxDecoration(border: Border.all()),
//                      child: Text('Hello'),
//                    ),
//                    Container(
//                      decoration: BoxDecoration(border: Border.all()),
//                      child: Text('Salam', style: TextStyle(fontSize: 39),),
//                    ),
//                  ],
//                ),
//              ],
//            ),
//          ),
          ShrinkWrappingViewport(
            axisDirection: AxisDirection.right,
            offset: ViewportOffset.zero(),
            slivers: [
              SliverToBoxAdapter(
                child: Container(
//                  height: 30,
                  decoration: BoxDecoration(border: Border.all()),
                  child: Text(
                    'Salam',
                    style: TextStyle(fontSize: 39),
                  ),
                ),
              ),
              SliverToBoxAdapter(
                child: Container(
                  decoration: BoxDecoration(border: Border.all()),
                  child: Text(
                    'Salam',
                    style: TextStyle(fontSize: 39),
                  ),
                ),
              )
            ],
          ),
          Container(
            color: Colors.redAccent,
            child: Text('Here ends'),
          )
        ],
      ),
    );
  }
}
